import org.gradle.api.tasks.testing.logging.TestExceptionFormat

plugins {
    java
    id("org.springframework.boot") version "3.3.1"
    id("io.spring.dependency-management") version "1.1.5"
    id("com.diffplug.spotless") version "6.25.0"
    id("org.asciidoctor.jvm.convert") version "3.3.2"
}

group = "com.rsupport.red"
version = "0.0.1"

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(21)
    }
}

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

repositories {
    mavenCentral()
}

val asciidoctorExt: Configuration by configurations.creating

dependencyManagement {
    imports {
        val springCloudVersion = "2023.0.3"
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:$springCloudVersion")
    }
}

val querydslVersion = dependencyManagement.importedProperties["querydsl.version"]

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-mail")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:2.6.0")
    implementation("org.springframework.cloud:spring-cloud-starter-config")
    implementation("org.apache.commons:commons-lang3")
    implementation("com.auth0:java-jwt:4.4.0")
    implementation("com.querydsl:querydsl-jpa:$querydslVersion:jakarta")
    annotationProcessor("com.querydsl:querydsl-apt:$querydslVersion:jakarta")
    annotationProcessor("jakarta.persistence:jakarta.persistence-api")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
    annotationProcessor("org.projectlombok:lombok")
    compileOnly("org.projectlombok:lombok")
    runtimeOnly("org.mariadb.jdbc:mariadb-java-client")
    testCompileOnly("org.projectlombok:lombok")
    testAnnotationProcessor("org.projectlombok:lombok")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.security:spring-security-test")
    testImplementation("org.springframework.restdocs:spring-restdocs-mockmvc")
    asciidoctorExt("org.springframework.restdocs:spring-restdocs-asciidoctor")
}

springBoot {
    buildInfo {
        properties {
            System.getenv("BUILD_NUMBER")?.let {
                additional.put("build_number", it)
            }
            System.getenv("COMMIT_HASH")?.let {
                additional.put("commit_hash", it)
            }
        }
    }
}

spotless {
    java {
        palantirJavaFormat("2.47.0")
    }
}

val initializeSchema by tasks.registering(Test::class) {
    useJUnitPlatform()
    include("**/InitializeSchema.class")
    doFirst { systemProperty("db.name", ext.get("dbName") as String) }
}

project.afterEvaluate {
    ext.set("dbName", System.getenv("DB_NAME") ?: "red_api")
    ext.set("dbAddress", System.getenv("DB_ADDRESS") ?: "localhost:13306")
}

val snippetsDir = file("build/generated-snippets")

tasks {
    test {
        useJUnitPlatform()
        exclude("**/*InitializeSchema.class")
        if (System.getProperty("db.init.skip") == null) {
            dependsOn(initializeSchema)
        }
        outputs.dir(snippetsDir)
    }
    withType<Test> {
        jvmArgs = listOf("-XX:+EnableDynamicAgentLoading")
        testLogging {
            showStandardStreams = true
            exceptionFormat = TestExceptionFormat.FULL
        }
    }
    processResources {
        filesMatching("**/application.yml") {
            expand(project.properties)
        }
    }
    jar {
        enabled = false
    }
    bootJar {
        dependsOn(asciidoctor)
        from("build/docs/asciidoc") {
            into("BOOT-INF/classes/static")
        }
    }
    asciidoctor {
        dependsOn(test)
        inputs.dir(snippetsDir)
        configurations(asciidoctorExt.name)
        baseDirFollowsSourceFile()
    }
}
