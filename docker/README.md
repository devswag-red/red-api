# docker

## docker-compose command

### 백그라운드에서 실행

```shell
$ docker-compose up -d
```

### 실행 종료

```shell
$ docker-compose stop
```

### 실행 종료 & 컨테이너 삭제

- 이미지는 삭제 안함

```shell
$ docker-compose down
```

### docs

- [Docker Compose overview](https://docs.docker.com/compose/)
