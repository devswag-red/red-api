package com.rsupport.red.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan("com.rsupport.red")
public class RedApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedApiApplication.class, args);
    }
}
