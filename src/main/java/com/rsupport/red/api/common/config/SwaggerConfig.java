package com.rsupport.red.api.common.config;

import io.swagger.v3.core.util.PrimitiveType;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityScheme;
import java.time.Instant;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;

@Configuration
@RequiredArgsConstructor
public class SwaggerConfig {

    public static final String SECURITY_SCHEME_KEY = "JWT";

    static {
        PrimitiveType.customClasses().put(Instant.class.getCanonicalName(), PrimitiveType.LONG);
    }

    private final BuildProperties buildProperties;

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI().info(info()).components(components());
    }

    private Info info() {
        return new Info().title("R.E.D").description("Rsupport Email Delivery").version(buildProperties.getVersion());
    }

    private Components components() {
        return new Components().addSecuritySchemes(SECURITY_SCHEME_KEY, securityScheme());
    }

    private SecurityScheme securityScheme() {
        return new SecurityScheme()
                .type(SecurityScheme.Type.APIKEY)
                .name(HttpHeaders.AUTHORIZATION)
                .in(SecurityScheme.In.HEADER);
    }
}
