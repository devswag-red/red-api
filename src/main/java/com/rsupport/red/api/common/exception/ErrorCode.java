package com.rsupport.red.api.common.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum ErrorCode {
    DATA_NOT_FOUND("9001", "data not found"),
    BAD_REQUEST("9002", "bad request"),
    ACCESS_DENIED("9003", "access denied"),
    EMPTY_ENABLED_MAIL_SERVICE_PROVIDER("9004", "Empty enabled mail service provider"),
    UNKNOWN("9999", "unknown exception"),
    USER_NOT_FOUND("9501", "user not found"),
    USER_PASSWORD_NOT_MATCHED("9502", "wrong user password"),
    USING_PASSWORD("9503", "use same password before"),
    PERMISSION_DENIED("9504", "user does not have permission"),
    DUPLICATE_USERNAME("9505", "username is already taken"),
    INVALID_TOKEN("9510", "invalid token");

    private final String code;
    private final String message;
}
