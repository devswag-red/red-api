package com.rsupport.red.api.common.jwt;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

@Slf4j
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final SimpleJwtUtil simpleJwtUtil;

    public JwtAuthenticationFilter(SimpleJwtUtil simpleJwtUtil) {
        this.simpleJwtUtil = simpleJwtUtil;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String token = simpleJwtUtil.resolveToken(request);
        if (StringUtils.isNotBlank(token)) {
            var decodedJWT = simpleJwtUtil.validateToken(token);
            var authenticationToken = simpleJwtUtil.getAuthentication(decodedJWT);
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }
        filterChain.doFilter(request, response);
    }
}
