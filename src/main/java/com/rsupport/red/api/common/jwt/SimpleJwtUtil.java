package com.rsupport.red.api.common.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.rsupport.red.api.common.exception.BaseException;
import com.rsupport.red.api.common.exception.ErrorCode;
import com.rsupport.red.api.common.properties.JwtProperties;
import com.rsupport.red.api.domain.User;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SimpleJwtUtil {

    private final JwtProperties jwtProperties;

    public String createToken(User user) {
        return createToken(user, jwtProperties.getExpirationTime());
    }

    private String createToken(User user, long expireTime) {

        Algorithm algorithm = Algorithm.HMAC512(jwtProperties.getSecretKey());
        Date expiresAt = new Date(System.currentTimeMillis() + expireTime);
        return JWT.create()
                .withSubject(user.getUsername())
                .withClaim("userId", user.getId())
                .withClaim("username", user.getUsername())
                .withClaim("role", user.getRole().name()) // username, role 필요한가..?
                .withIssuedAt(new Date())
                .withExpiresAt(expiresAt)
                .sign(algorithm);
    }

    public String resolveToken(HttpServletRequest request) {
        return request.getHeader(HttpHeaders.AUTHORIZATION);
    }

    public DecodedJWT validateToken(String token) {
        try {
            return JWT.require(Algorithm.HMAC512(jwtProperties.getSecretKey()))
                    .build()
                    .verify(token);
        } catch (Exception e) {
            throw new BaseException(ErrorCode.INVALID_TOKEN);
        }
    }

    public Authentication getAuthentication(DecodedJWT decodedJWT) {
        String userId = decodedJWT.getClaim("userId").asString();
        String role = decodedJWT.getClaim("role").toString();
        Set<GrantedAuthority> authorities = new LinkedHashSet<>();
        authorities.add((GrantedAuthority) () -> role);

        return new JwtAuthenticationToken(userId, null, authorities);
    }
}
