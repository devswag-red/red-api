package com.rsupport.red.api.common.exception;

import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.resource.NoResourceFoundException;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleNoResourceFoundException(NoResourceFoundException e) {
        printWarnLog(ErrorCode.BAD_REQUEST, e.getMessage());
        return new ResponseEntity<>(ErrorResponse.from(ErrorCode.BAD_REQUEST), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleBindException(BindException e) {
        var message = convertToMessage(e);
        printWarnLog(ErrorCode.BAD_REQUEST, message);
        return new ResponseEntity<>(ErrorResponse.of(ErrorCode.BAD_REQUEST, message), HttpStatus.BAD_REQUEST);
    }

    private String convertToMessage(BindException e) {
        var fieldErrors = e.getBindingResult().getFieldErrors();
        return fieldErrors.stream()
                .map(it -> String.format("%s: %s", it.getField(), StringUtils.defaultString(it.getDefaultMessage())))
                .collect(Collectors.joining(", "));
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleBaseException(BaseException e) {
        printWarnLog(e.getErrorCode(), e.getMessage());
        return new ResponseEntity<>(ErrorResponse.from(e.getErrorCode()), HttpStatus.BAD_REQUEST);
    }

    private void printWarnLog(ErrorCode errorCode, String message) {
        log.warn("errorCode: {}, message: {}", errorCode.getCode(), message);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleException(Exception e) {
        log.error(String.format("errorCode: %s, message: %s", ErrorCode.UNKNOWN.getCode(), e.getMessage()), e);
        return new ResponseEntity<>(ErrorResponse.from(ErrorCode.UNKNOWN), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
