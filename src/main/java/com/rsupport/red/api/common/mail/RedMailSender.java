package com.rsupport.red.api.common.mail;

import java.util.Map;
import java.util.Properties;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RedMailSender {

    public SendResultDto send(ProviderDto providerDto, MessageDto messageDto) {
        JavaMailSenderImpl sender = getSender(providerDto);
        try {
            send(sender, messageDto);
        } catch (MailException mailException) {
            return SendResultDto.failed(mailException);
        }

        return SendResultDto.succeeded();
    }

    private JavaMailSenderImpl getSender(ProviderDto providerDto) {
        MailProperties mailProperties = new MailProperties();
        providerDto.copyTo(mailProperties);

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(mailProperties.getHost());
        mailSender.setPort(mailProperties.getPort());
        mailSender.setUsername(mailProperties.getUsername());
        mailSender.setPassword(mailProperties.getPassword());

        mailSender.setProtocol(mailProperties.getProtocol());
        mailSender.setDefaultEncoding(mailProperties.getDefaultEncoding().name());

        mailSender.setJavaMailProperties(asProperties(mailProperties.getProperties()));

        return mailSender;
    }

    private Properties asProperties(Map<String, String> source) {
        source.put("mail.smtp.auth", "true");
        source.put("mail.smtp.auth.login.disable", "true");
        source.put("mail.smtp.starttls.enable", "true"); // required for OCI
        source.put("mail.smtp.starttls.required", "true");
        source.put("mail.smtp.ssl.protocols", "TLSv1.2");

        Properties properties = new Properties();
        properties.putAll(source);
        return properties;
    }

    private void send(JavaMailSenderImpl sender, MessageDto messageDto) {
        sender.send(mimeMessage -> {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
            helper.setFrom(messageDto.sender());
            helper.setTo(messageDto.receiver());
            helper.setSubject(messageDto.subject());
            helper.setText(messageDto.text(), true);
        });
    }
}
