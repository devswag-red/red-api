package com.rsupport.red.api.common.mail;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ResultCode {
    SUCCESS("200", "Success"),
    AUTHENTICATION_FAILED("300", "Authentication failed"),
    UNSENT("400", "Receiving failed"),
    INVALIDED("500", "Receiving failed"),
    UNKNOWN("999", "Unknown error");

    private final String code;
    private final String description;
}
