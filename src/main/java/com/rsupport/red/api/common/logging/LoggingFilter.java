package com.rsupport.red.api.common.logging;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE + 1)
@Slf4j
public class LoggingFilter extends OncePerRequestFilter {

    private static final String[] IGNORED_URI_PATHS = {
        "/api-docs", "/swagger", "/version.txt", "/favicon.ico", "/api.html"
    };

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return Arrays.stream(IGNORED_URI_PATHS)
                .anyMatch(path -> request.getRequestURI().startsWith(path));
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        var cachingRequestWrapper = new ContentCachingRequestWrapper(request);
        var cachingResponseWrapper = new ContentCachingResponseWrapper(response);

        var stopWatch = new StopWatch();
        stopWatch.start();

        try {
            filterChain.doFilter(cachingRequestWrapper, cachingResponseWrapper);
        } finally {
            stopWatch.stop();
            log.info(printLog(cachingRequestWrapper, cachingResponseWrapper, stopWatch.getTotalTimeMillis()));
        }

        cachingResponseWrapper.copyBodyToResponse();
    }

    private String printLog(ContentCachingRequestWrapper request, ContentCachingResponseWrapper response, long time) {
        return String.format(
                """
                |
                |%s %s (%s) (%dms)
                |>> request parameter: %s
                |>> request body: %s
                |>> response body: %s
                """
                        .trim(),
                request.getMethod(),
                request.getRequestURI(),
                HttpStatus.valueOf(response.getStatus()),
                time,
                parameters(request),
                requestBody(request),
                responseBody(response));
    }

    private String parameters(ContentCachingRequestWrapper request) {
        return request.getParameterMap().entrySet().stream()
                .map(entry -> entry.getKey() + "=" + String.join(",", entry.getValue()))
                .collect(Collectors.joining("&"));
    }

    private String requestBody(ContentCachingRequestWrapper request) {
        return request.getContentAsString();
    }

    private String responseBody(ContentCachingResponseWrapper response) {
        return new String(response.getContentAsByteArray(), StandardCharsets.UTF_8);
    }
}
