package com.rsupport.red.api.common.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ColumnSize {

    public static final int UUID = 50;
    public static final int NAME = 50;
    public static final int SECRET = 36;
    public static final int PASSWORD = 100;
    public static final int USERNAME = 50;
    public static final int CODE1 = 1;
    public static final int CODE2 = 2;
    public static final int CODE10 = 10;
    public static final int CODE20 = 20;
}
