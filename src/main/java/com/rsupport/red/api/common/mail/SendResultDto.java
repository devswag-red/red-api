package com.rsupport.red.api.common.mail;

import jakarta.mail.SendFailedException;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.MailException;
import org.springframework.mail.MailParseException;
import org.springframework.mail.MailPreparationException;
import org.springframework.mail.MailSendException;

public record SendResultDto(boolean success, MailException mailException) {

    public static SendResultDto succeeded() {
        return new SendResultDto(true, null);
    }

    public static SendResultDto failed(MailException mailException) {
        return new SendResultDto(false, mailException);
    }

    public boolean shouldResend() {
        return isFailed() && isMailAuthenticationException();
    }

    public boolean isFailed() {
        return !success;
    }

    public boolean isMailAuthenticationException() {
        return mailException != null && mailException instanceof MailAuthenticationException;
    }

    public boolean isInvalidedException() {
        if (mailException == null) {
            return false;
        }

        if (mailException instanceof MailParseException || mailException instanceof MailPreparationException) {
            return true;
        }

        if (!(mailException instanceof MailSendException mailSendException)) {
            return false;
        }

        for (Exception exception : mailSendException.getFailedMessages().values()) {
            if (!(exception instanceof SendFailedException sendFailedException)) {
                continue;
            }

            return sendFailedException.getInvalidAddresses() != null
                    && sendFailedException.getInvalidAddresses().length > 0;
        }

        return false;
    }

    public boolean isUnsentException() {
        if (mailException == null) {
            return false;
        }

        if (!(mailException instanceof MailSendException mailSendException)) {
            return false;
        }

        for (Exception exception : mailSendException.getFailedMessages().values()) {
            if (!(exception instanceof SendFailedException sendFailedException)) {
                continue;
            }

            return sendFailedException.getValidUnsentAddresses() != null
                    && sendFailedException.getValidUnsentAddresses().length > 0;
        }

        return false;
    }

    public ResultCode getResultCode() {
        if (success) {
            return ResultCode.SUCCESS;
        } else if (isMailAuthenticationException()) {
            return ResultCode.AUTHENTICATION_FAILED;
        } else if (isUnsentException()) {
            return ResultCode.UNSENT;
        } else if (isInvalidedException()) {
            return ResultCode.INVALIDED;
        } else {
            return ResultCode.UNKNOWN;
        }
    }
}
