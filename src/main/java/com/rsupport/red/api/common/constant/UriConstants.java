package com.rsupport.red.api.common.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class UriConstants {

    // USER
    public static final String USER_ROOT = "/user";
    public static final String USER_PASSWORD = "/password";

    public static final String AUTHENTICATE_ROOT = "/authenticate";
}
