package com.rsupport.red.api.common.version;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import javax.sql.DataSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.jdbc.support.MetaDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

@Component
@RequiredArgsConstructor
@Slf4j
public class VersionLogger implements ApplicationRunner {

    private final DataSource dataSource;

    @Override
    public void run(ApplicationArguments args) {
        logBuildVersion();
        logDataBaseVersion();
    }

    private void logBuildVersion() {
        var buildProperties = new ClassPathResource("META-INF/build-info.properties");
        try {
            if (buildProperties.exists()) {
                String buildInfo = StreamUtils.copyToString(buildProperties.getInputStream(), StandardCharsets.UTF_8);
                log.info("buildVersion\n{}", buildInfo.substring(0, buildInfo.length() - 1));
            }
        } catch (IOException e) {
            // Ignore
        }
    }

    private void logDataBaseVersion() {
        try {
            var map = JdbcUtils.extractDatabaseMetaData(
                    dataSource,
                    it -> Map.of("vendor", it.getDatabaseProductName(), "version", it.getDatabaseProductVersion()));

            log.info("databaseVersion: {}", map);
        } catch (MetaDataAccessException e) {
            // Ignore
        }
    }
}
