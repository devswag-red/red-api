package com.rsupport.red.api.common.mail;

public record MessageDto(String sender, String receiver, String subject, String text) {

    public static MessageDto create(String sender, String receiver, String subject, String text) {
        return new MessageDto(sender, receiver, subject, text);
    }
}
