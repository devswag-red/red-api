package com.rsupport.red.api.common.mail;

import org.springframework.boot.autoconfigure.mail.MailProperties;

public record ProviderDto(String host, int port, String username, String password) {

    public static ProviderDto create(String host, int port, String username, String password) {
        return new ProviderDto(host, port, username, password);
    }

    public void copyTo(MailProperties target) {
        target.setHost(host);
        target.setPort(port);
        target.setUsername(username);
        target.setPassword(password);
    }
}
