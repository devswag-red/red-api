package com.rsupport.red.api.common.config;

import com.rsupport.red.api.common.exception.BaseException;
import com.rsupport.red.api.common.exception.ErrorCode;
import com.rsupport.red.api.common.jwt.JwtAuthenticationFilter;
import com.rsupport.red.api.common.jwt.SimpleJwtUtil;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.RequestCacheConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@AllArgsConstructor
public class SecurityConfig {

    private final SimpleJwtUtil simpleJwtUtil;

    private static final String[] IGNORE_URI_PATTERNS = {
        "/authenticate",
        "/api-docs/**",
        "/swagger-ui/**",
        "/swagger",
        "/version.txt",
        "/error",
        "/favicon.ico",
        "/api.html",
        "/mail-send"
    };

    @Bean
    @Order(0)
    public SecurityFilterChain ignoredPatternFilterChain(HttpSecurity http) throws Exception {
        return http.securityMatchers(matchers -> matchers.requestMatchers(IGNORE_URI_PATTERNS))
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(authorize -> authorize.anyRequest().permitAll())
                .cors(AbstractHttpConfigurer::disable)
                .requestCache(RequestCacheConfigurer::disable)
                .securityContext(AbstractHttpConfigurer::disable)
                .sessionManagement(AbstractHttpConfigurer::disable)
                .build();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return http.csrf(AbstractHttpConfigurer::disable)
                .cors(AbstractHttpConfigurer::disable)
                .httpBasic(AbstractHttpConfigurer::disable)
                .formLogin(AbstractHttpConfigurer::disable)
                .logout(AbstractHttpConfigurer::disable)
                .sessionManagement(it -> it.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .authorizeHttpRequests(auth -> auth.anyRequest().authenticated())
                .exceptionHandling(it -> it.authenticationEntryPoint((request, response, authException) -> {
                    throw new BaseException(ErrorCode.ACCESS_DENIED);
                }))
                .addFilterBefore(new JwtAuthenticationFilter(simpleJwtUtil), UsernamePasswordAuthenticationFilter.class)
                .build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }
}
