package com.rsupport.red.api.common.exception;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class GlobalErrorController implements ErrorController {

    @GetMapping("/error")
    public String handleError(HttpServletRequest request) throws Exception {
        throw (Exception) request.getAttribute(RequestDispatcher.ERROR_EXCEPTION);
    }
}
