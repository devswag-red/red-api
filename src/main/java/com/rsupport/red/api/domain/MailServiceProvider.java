package com.rsupport.red.api.domain;

import com.rsupport.red.api.domain.eums.Provider;
import com.rsupport.red.base.data.BaseTraceableEntity;
import jakarta.persistence.*;
import java.util.Objects;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UuidGenerator;

@Entity
@Table(name = "rt_mail_service_provider")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class MailServiceProvider extends BaseTraceableEntity {

    @Id
    @UuidGenerator
    private String id;

    @Enumerated(EnumType.STRING)
    @Column(length = 50, nullable = false)
    private Provider provider;

    @Column(length = 200, nullable = false)
    private String username;

    @Column(length = 100, nullable = false)
    private String password;

    @Column(length = 200, nullable = false)
    private String host;

    @Column(length = 30, nullable = false)
    private String port;

    @ColumnDefault("-1")
    @Column(nullable = false)
    private int priority = -1;

    @ColumnDefault("1")
    @Column(nullable = false)
    private boolean enabled = true;

    @Column(length = 50, nullable = false)
    private String sender;

    @ColumnDefault("0")
    @Column(nullable = false)
    private int failCount = 0;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Domain domain;

    private static final int MAX_MAIL_SEND_FAILED_COUNT = 5;

    public static MailServiceProvider of(
            Provider provider,
            String username,
            String password,
            String host,
            String port,
            int priority,
            Boolean enabled,
            String sender,
            Domain domain) {
        MailServiceProvider mailServiceProvider = new MailServiceProvider();
        mailServiceProvider.provider = provider;
        mailServiceProvider.username = username;
        mailServiceProvider.password = password;
        mailServiceProvider.host = host;
        mailServiceProvider.port = port;
        mailServiceProvider.priority = priority;
        mailServiceProvider.enabled = enabled;
        mailServiceProvider.sender = sender;
        mailServiceProvider.domain = domain;

        return mailServiceProvider;
    }

    public void update(
            Provider provider,
            String username,
            String password,
            String host,
            String port,
            int priority,
            Boolean enabled,
            String sender,
            Domain domain) {
        this.provider = provider;
        this.username = username;
        this.password = password;
        this.host = host;
        this.port = port;
        this.priority = priority;
        this.enabled = enabled;
        this.sender = sender;
        this.domain = domain;

        updateEnabled(enabled);
    }

    public void updateEnabled(boolean enabled) {
        this.enabled = enabled;
        if (enabled) {
            this.clearFailCount();
        }
    }

    public void increaseFailCount() {
        this.failCount++;
        if (this.failCount > MAX_MAIL_SEND_FAILED_COUNT) {
            updateEnabled(false);
        }
    }

    public void clearFailCount() {
        this.failCount = 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MailServiceProvider that = (MailServiceProvider) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MailServiceProvider{" + "id='"
                + id + '\'' + ", provider="
                + provider + ", username='"
                + username + '\'' + ", password='"
                + password + '\'' + ", host='"
                + host + '\'' + ", port='"
                + port + '\'' + ", priority="
                + priority + ", enabled="
                + enabled + ", sender='"
                + sender + '\'' + ", failCount="
                + failCount + '}';
    }
}
