package com.rsupport.red.api.domain;

import com.rsupport.red.api.common.constant.ColumnSize;
import com.rsupport.red.base.data.BaseCreateEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UuidGenerator;

@Entity
@Table(name = "rt_domain")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Domain extends BaseCreateEntity {

    @Id
    @UuidGenerator
    @Column(length = ColumnSize.UUID)
    @EqualsAndHashCode.Include
    private String id;

    @Column(length = ColumnSize.NAME, nullable = false, unique = true)
    private String name;

    public static Domain from(String name) {
        Domain domain = new Domain();
        domain.name = name;
        return domain;
    }

    public void updateName(String name) {
        this.name = name;
    }
}
