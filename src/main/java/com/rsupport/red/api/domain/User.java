package com.rsupport.red.api.domain;

import com.rsupport.red.api.common.constant.ColumnSize;
import com.rsupport.red.api.domain.enums.Role;
import com.rsupport.red.base.data.BaseTraceableEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.UuidGenerator;
import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "rt_user", uniqueConstraints = @UniqueConstraint(columnNames = "username"))
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Setter
@Getter
@ToString
public class User extends BaseTraceableEntity {

    @Id
    @UuidGenerator
    private String id;

    @Column(length = ColumnSize.USERNAME, nullable = false)
    private String username;

    @Column(length = ColumnSize.NAME, nullable = false)
    private String name;

    @ToString.Exclude
    @Column(length = ColumnSize.PASSWORD, nullable = false)
    private String password;

    @Column(name = "role_code", length = ColumnSize.CODE10, nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    public static User of(String username, String name, String password, Long roleCode) {
        User user = new User();
        user.username = username;
        user.name = name;
        user.password = password;
        user.role = Role.of(roleCode);
        return user;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> authorities = new LinkedHashSet<>();
        authorities.add((GrantedAuthority) () -> role.name());
        return authorities;
    }

    public boolean isAdmin() {
        return role.equals(Role.ADMIN);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        User user = (User) o;
        return username.equals(user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(username);
    }
}
