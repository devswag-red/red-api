package com.rsupport.red.api.domain;

import com.rsupport.red.api.common.constant.ColumnSize;
import com.rsupport.red.base.data.BaseCreateEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.Objects;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.UuidGenerator;

@Entity
@Table(name = "lt_mail_send")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@ToString
public class MailSendLog extends BaseCreateEntity {

    @Id
    @UuidGenerator
    @Column(length = ColumnSize.UUID)
    private String id;

    @Column(length = ColumnSize.UUID, nullable = false)
    private String requestId;

    @Column(length = 50, nullable = false)
    private String sender;

    @Column(length = 50, nullable = false)
    private String receiver;

    @Column(nullable = false)
    @ColumnDefault("0")
    private boolean success;

    @Column(length = ColumnSize.UUID, nullable = false)
    private String domainId;

    // TODO: domain 이름과 provider 타입 > MailSendLog 에 저장할지, 실시간으로 테이블에서 조회할지 검토
    @Column(length = ColumnSize.UUID, nullable = false)
    private String domainName;

    @Column(length = ColumnSize.UUID, nullable = false)
    private String providerId;

    // TODO: enum 으로 변경 예정
    @Column(length = ColumnSize.UUID, nullable = false)
    private String providerType;

    // TODO: 에러 정보를 어떻게 남길지 논의 필요
    @Column
    private String failCode;

    @Column
    private String failMessage;

    public MailSendLog(
            String requestId,
            String sender,
            String receiver,
            boolean success,
            String domainId,
            String domainName,
            String providerId,
            String providerType,
            String failCode,
            String failMessage) {
        this.requestId = requestId;
        this.sender = sender;
        this.receiver = receiver;
        this.success = success;
        this.domainId = domainId;
        this.domainName = domainName;
        this.providerId = providerId;
        this.providerType = providerType;
        this.failCode = failCode;
        this.failMessage = failMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MailSendLog mailSendLog = (MailSendLog) o;
        return id.equals(mailSendLog.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
