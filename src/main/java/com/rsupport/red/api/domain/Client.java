package com.rsupport.red.api.domain;

import com.rsupport.red.api.common.constant.ColumnSize;
import com.rsupport.red.base.data.BaseCreateTraceableEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UuidGenerator;

@Entity
@Table(name = "rt_client")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Client extends BaseCreateTraceableEntity {

    @Id
    @UuidGenerator
    @Column(length = ColumnSize.UUID)
    @EqualsAndHashCode.Include
    private String id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Domain domain;

    @Column(length = ColumnSize.SECRET, nullable = false, unique = true)
    private String secret = UUID.randomUUID().toString();

    @Column
    private boolean enabled = true;

    public static Client create(Domain domain, boolean enabled) {
        Client client = new Client();
        client.changeDomain(domain, enabled);
        return client;
    }

    public void changeDomain(Domain domain, boolean enabled) {
        this.domain = domain;
        if (enabled) {
            enable();
        } else {
            disable();
        }
    }

    private void enable() {
        enabled = true;
    }

    private void disable() {
        enabled = false;
    }

    public String getDomainName() {
        return domain.getName();
    }
}
