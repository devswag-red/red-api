package com.rsupport.red.api.service;

import com.rsupport.red.api.common.exception.BaseException;
import com.rsupport.red.api.common.exception.ErrorCode;
import com.rsupport.red.api.common.jwt.SimpleJwtUtil;
import com.rsupport.red.api.controller.dto.request.DeleteUserRequest;
import com.rsupport.red.api.controller.dto.request.UpdatePasswordRequest;
import com.rsupport.red.api.controller.dto.request.UpdateUserRequest;
import com.rsupport.red.api.controller.dto.response.UserResponse;
import com.rsupport.red.api.domain.User;
import com.rsupport.red.api.domain.enums.Role;
import com.rsupport.red.api.repository.UserRepository;
import com.rsupport.red.base.data.GenericService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserService extends GenericService<User, String, UserRepository> {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final SimpleJwtUtil simpleJwtUtil;

    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(() -> new BaseException(ErrorCode.USER_NOT_FOUND));
    }

    public UserResponse getUser(String userId) {
        return UserResponse.of(
                userRepository.findById(userId).orElseThrow(() -> new BaseException(ErrorCode.USER_NOT_FOUND)));
    }

    public List<UserResponse> getUserList(Pageable pageable) {
        List<User> userList = userRepository.findAll(pageable).getContent();
        return userList.stream().map(UserResponse::of).collect(Collectors.toList());
    }

    @Transactional
    public UserResponse saveUser(User user) {
        return UserResponse.of(userRepository.save(user));
    }

    public String authenticate(String username, String password) {
        User user = getUserByUsername(username);
        checkPassword(password, user.getPassword());
        return simpleJwtUtil.createToken(user);
    }

    public void checkPassword(String currentPassword, String password) {
        if (!passwordEncoder.matches(currentPassword, password)) {
            throw new BaseException(ErrorCode.USER_PASSWORD_NOT_MATCHED);
        }
    }

    @Transactional
    public UserResponse updateUser(String userId, UpdateUserRequest updateUserRequest) {
        User user = userRepository.findById(userId).orElseThrow(() -> new BaseException(ErrorCode.USER_NOT_FOUND));
        user.setUsername(updateUserRequest.username());
        user.setName(updateUserRequest.name());
        user.setRole(Role.of(updateUserRequest.role_code()));
        return saveUser(user);
    }

    @Transactional
    public UserResponse updatePassword(UpdatePasswordRequest updatePasswordRequest) {
        User user = userRepository
                .findById(updatePasswordRequest.id())
                .orElseThrow(() -> new BaseException(ErrorCode.USER_NOT_FOUND));

        checkPasswordUpdatePossible(user, updatePasswordRequest);

        user.setPassword(passwordEncoder.encode(updatePasswordRequest.newPassword()));
        return saveUser(user);
    }

    @Transactional
    public void deleteUser(DeleteUserRequest deleteUserRequest) {
        User targetUser = userRepository
                .findById(deleteUserRequest.targetId())
                .orElseThrow(() -> new BaseException(ErrorCode.USER_NOT_FOUND));
        userRepository.delete(targetUser);
    }

    private void checkPasswordUpdatePossible(User user, UpdatePasswordRequest updatePasswordRequest) {
        checkPassword(updatePasswordRequest.currentPassword(), user.getPassword());
        if (updatePasswordRequest.currentPassword().equals(updatePasswordRequest.newPassword())) {
            throw new BaseException(ErrorCode.USING_PASSWORD);
        }
    }

    public boolean checkExistUsername(String username) {
        return userRepository.findByUsername(username).isPresent();
    }
}
