package com.rsupport.red.api.service;

import static java.util.Comparator.comparing;

import com.rsupport.red.api.controller.dto.DomainCreateRequest;
import com.rsupport.red.api.controller.dto.DomainRequest;
import com.rsupport.red.api.controller.dto.DomainResponse;
import com.rsupport.red.api.controller.dto.DomainUpdateRequest;
import com.rsupport.red.api.domain.Domain;
import com.rsupport.red.api.repository.DomainRepository;
import com.rsupport.red.base.data.GenericService;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DomainService extends GenericService<Domain, String, DomainRepository> {

    public List<DomainResponse> findAll(DomainRequest request) {
        return repository.findAll().stream()
                .filter(domain -> matchKeyword(domain, request))
                .map(DomainResponse::from)
                .sorted(comparing(DomainResponse::createdTime).reversed())
                .toList();
    }

    private boolean matchKeyword(Domain domain, DomainRequest request) {
        String keyword = request.keyword();
        if (StringUtils.isBlank(keyword)) {
            return true;
        }

        return switch (request.target()) {
            case ID -> StringUtils.equals(keyword, domain.getId());
            case NAME -> StringUtils.equals(keyword, domain.getName());
        };
    }

    @Transactional
    public DomainResponse save(DomainCreateRequest request) {
        Domain newDomain = Domain.from(request.name());

        return DomainResponse.from(repository.save(newDomain));
    }

    @Transactional
    public DomainResponse update(String id, DomainUpdateRequest request) {
        Domain domain = get(id);
        domain.updateName(request.name());
        return DomainResponse.from(domain);
    }
}
