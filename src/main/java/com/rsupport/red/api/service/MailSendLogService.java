package com.rsupport.red.api.service;

import com.rsupport.red.api.controller.dto.MailSendLogRequest;
import com.rsupport.red.api.domain.MailSendLog;
import com.rsupport.red.api.repository.MailSendLogRepository;
import com.rsupport.red.base.data.GenericService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MailSendLogService extends GenericService<MailSendLog, String, MailSendLogRepository> {

    public List<MailSendLog> getList(MailSendLogRequest request) {
        return repository.getList(request);
    }
}
