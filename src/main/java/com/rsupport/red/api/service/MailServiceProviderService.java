package com.rsupport.red.api.service;

import static java.util.Comparator.comparing;

import com.rsupport.red.api.common.exception.BaseException;
import com.rsupport.red.api.common.exception.ErrorCode;
import com.rsupport.red.api.controller.dto.MailServiceProviderListRequest;
import com.rsupport.red.api.controller.dto.MailServiceProviderRequest;
import com.rsupport.red.api.controller.dto.MailServiceProviderResponse;
import com.rsupport.red.api.domain.Domain;
import com.rsupport.red.api.domain.MailServiceProvider;
import com.rsupport.red.api.repository.DomainRepository;
import com.rsupport.red.api.repository.MailServiceProviderRepository;
import com.rsupport.red.base.data.GenericService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class MailServiceProviderService
        extends GenericService<MailServiceProvider, String, MailServiceProviderRepository> {

    private final DomainRepository domainRepository;

    @Transactional
    public String save(MailServiceProviderRequest request) {
        var domain = getDomain(request.domainId());
        var mailServiceProvider = MailServiceProvider.of(
                request.provider(),
                request.username(),
                request.password(),
                request.host(),
                request.port(),
                request.priority(),
                request.enabled(),
                request.sender(),
                domain);
        return save(mailServiceProvider).getId();
    }

    private Domain getDomain(String domainId) {
        return domainRepository.findById(domainId).orElseThrow(() -> new BaseException(ErrorCode.DATA_NOT_FOUND));
    }

    public MailServiceProviderResponse getDetail(String id) {
        var mailServiceProvider = get(id);
        return MailServiceProviderResponse.of(mailServiceProvider);
    }

    @Transactional
    public void update(String id, MailServiceProviderRequest request) {
        var mailServiceProvider = get(id);
        var domain = getDomain(request.domainId());

        mailServiceProvider.update(
                request.provider(),
                request.username(),
                request.password(),
                request.host(),
                request.port(),
                request.priority(),
                request.enabled(),
                request.sender(),
                domain);
    }

    public List<MailServiceProviderResponse> getList(MailServiceProviderListRequest request) {
        return repository.findAll().stream()
                .filter(mailServiceProvider ->
                        matchKeyword(mailServiceProvider, request.searchTarget(), request.keyword()))
                .map(MailServiceProviderResponse::of)
                .sorted(comparing(MailServiceProviderResponse::createdTime).reversed())
                .toList();
    }

    private boolean matchKeyword(
            MailServiceProvider mailServiceProvider,
            MailServiceProviderListRequest.SearchTarget searchTarget,
            String keyword) {
        if (searchTarget == null || StringUtils.isBlank(keyword)) {
            return true;
        }

        return switch (searchTarget) {
            case MailServiceProviderListRequest.SearchTarget.ID -> StringUtils.containsIgnoreCase(
                    keyword, mailServiceProvider.getId());
            case MailServiceProviderListRequest.SearchTarget.DOMAIN -> StringUtils.equalsIgnoreCase(
                    keyword, mailServiceProvider.getDomain().getName());
            case MailServiceProviderListRequest.SearchTarget.PROVIDER -> StringUtils.equalsIgnoreCase(
                    keyword, mailServiceProvider.getProvider().name());
            case MailServiceProviderListRequest.SearchTarget.USERNAME -> StringUtils.containsIgnoreCase(
                    keyword, mailServiceProvider.getUsername());
        };
    }

    public List<MailServiceProvider> getEnabled(String domainId) {
        return repository.findByDomainIdAndEnabledTrueOrderByPriority(domainId);
    }

    private MailServiceProvider getLocked(String id) {
        return repository.findForUpdateById(id).orElseThrow(() -> new BaseException(ErrorCode.DATA_NOT_FOUND));
    }

    @Transactional
    public void clearFailCount(String id) {
        var lockedProvider = getLocked(id);
        lockedProvider.clearFailCount();
    }

    @Transactional
    public void increaseFailCount(String id) {
        var lockedProvider = getLocked(id);
        lockedProvider.increaseFailCount();
    }
}
