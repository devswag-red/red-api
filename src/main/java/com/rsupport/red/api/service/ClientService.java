package com.rsupport.red.api.service;

import static java.util.Comparator.comparing;

import com.rsupport.red.api.common.exception.BaseException;
import com.rsupport.red.api.common.exception.ErrorCode;
import com.rsupport.red.api.controller.dto.ClientCreateRequest;
import com.rsupport.red.api.controller.dto.ClientRequest;
import com.rsupport.red.api.controller.dto.ClientResponse;
import com.rsupport.red.api.controller.dto.ClientUpdateRequest;
import com.rsupport.red.api.domain.Client;
import com.rsupport.red.api.domain.Domain;
import com.rsupport.red.api.repository.ClientRepository;
import com.rsupport.red.api.repository.DomainRepository;
import com.rsupport.red.base.data.GenericService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ClientService extends GenericService<Client, String, ClientRepository> {

    private final DomainRepository domainRepository;

    public List<ClientResponse> findAll(ClientRequest request) {
        return repository.findAll().stream()
                .filter(client -> matchKeyword(client, request))
                .map(ClientResponse::from)
                .sorted(comparing(ClientResponse::createdTime).reversed())
                .toList();
    }

    public Client getClient(String secret) {
        return repository.findBySecret(secret).orElseThrow(() -> new BaseException(ErrorCode.DATA_NOT_FOUND));
    }

    private boolean matchKeyword(Client client, ClientRequest request) {
        String keyword = request.keyword();
        if (StringUtils.isBlank(keyword)) {
            return true;
        }

        return switch (request.target()) {
            case ID -> StringUtils.equals(keyword, client.getId());
            case SECRET -> StringUtils.equals(keyword, client.getSecret());
            case DOMAIN -> StringUtils.equals(keyword, client.getDomainName());
        };
    }

    @Override
    public Client get(String id) {
        return repository.findById(id).orElseThrow(() -> new BaseException(ErrorCode.DATA_NOT_FOUND));
    }

    @Transactional
    public ClientResponse save(ClientCreateRequest request) {
        Domain domain = getDomain(request.domainId());
        Client newClient = Client.create(domain, request.enabled());

        return ClientResponse.from(repository.save(newClient));
    }

    @Transactional
    public ClientResponse update(String id, ClientUpdateRequest request) {
        Client client = get(id);

        Domain domain = getDomain(request.domainId());
        client.changeDomain(domain, request.enabled());
        return ClientResponse.from(client);
    }

    private Domain getDomain(String domainId) {
        return domainRepository.findById(domainId).orElseThrow(() -> new BaseException(ErrorCode.DATA_NOT_FOUND));
    }
}
