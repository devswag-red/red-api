package com.rsupport.red.api.controller.dto;

import com.rsupport.red.api.domain.MailSendLog;
import java.time.Instant;

public record MailSendLogResponse(
        Instant createdTime,
        String domainName,
        String providerType,
        String sender,
        String receiver,
        boolean success,
        String failCode,
        String failMessage) {

    public static MailSendLogResponse of(MailSendLog mailSendLog) {
        return new MailSendLogResponse(
                mailSendLog.getCreatedAt(),
                mailSendLog.getDomainName(),
                mailSendLog.getProviderType(),
                mailSendLog.getSender(),
                mailSendLog.getReceiver(),
                mailSendLog.isSuccess(),
                mailSendLog.getFailCode(),
                mailSendLog.getFailMessage());
    }
}
