package com.rsupport.red.api.controller.dto;

import com.rsupport.red.api.domain.eums.Provider;

public record MailServiceProviderRequest(
        String domainId,
        Provider provider,
        String username,
        String password,
        String host,
        String port,
        int priority,
        boolean enabled,
        String sender) {}
