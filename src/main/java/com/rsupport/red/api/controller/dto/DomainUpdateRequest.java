package com.rsupport.red.api.controller.dto;

public record DomainUpdateRequest(String name) {}
