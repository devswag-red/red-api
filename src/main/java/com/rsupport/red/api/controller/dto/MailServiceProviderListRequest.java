package com.rsupport.red.api.controller.dto;

public record MailServiceProviderListRequest(SearchTarget searchTarget, String keyword) {

    public enum SearchTarget {
        ID,
        DOMAIN,
        PROVIDER,
        USERNAME
    }
}
