package com.rsupport.red.api.controller.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(description = "사용자 비밀번호 변경 요청 DTO")
public record UpdatePasswordRequest(
        @Schema(description = "변경할 사용자 ID", defaultValue = "user02") @NotBlank(message = "ID") String id,
        @Schema(description = "현재 비밀번호", defaultValue = "111111") @NotBlank(message = "현재 비밀번호") String currentPassword,
        @Schema(description = "변경할 비밀번호", defaultValue = "777777") @NotBlank(message = "새 비밀번호") String newPassword) {}
