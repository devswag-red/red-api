package com.rsupport.red.api.controller.dto;

public record ClientUpdateRequest(String domainId, boolean enabled) {}
