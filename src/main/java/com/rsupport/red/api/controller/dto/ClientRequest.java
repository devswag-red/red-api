package com.rsupport.red.api.controller.dto;

public record ClientRequest(SearchTarget target, String keyword) {

    public enum SearchTarget {
        ID,
        DOMAIN,
        SECRET
    }
}
