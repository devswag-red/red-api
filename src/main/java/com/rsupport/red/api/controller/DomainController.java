package com.rsupport.red.api.controller;

import com.rsupport.red.api.common.config.SwaggerConfig;
import com.rsupport.red.api.controller.dto.DomainCreateRequest;
import com.rsupport.red.api.controller.dto.DomainRequest;
import com.rsupport.red.api.controller.dto.DomainResponse;
import com.rsupport.red.api.controller.dto.DomainUpdateRequest;
import com.rsupport.red.api.service.DomainService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/domain")
@RestController
@RequiredArgsConstructor
@SecurityRequirement(name = SwaggerConfig.SECURITY_SCHEME_KEY)
public class DomainController {

    private final DomainService domainService;

    @GetMapping
    public HttpEntity<List<DomainResponse>> content(DomainRequest request) {
        return ResponseEntity.ok(domainService.findAll(request));
    }

    @GetMapping("/{id}")
    public HttpEntity<DomainResponse> detail(@PathVariable String id) {
        return ResponseEntity.ok(DomainResponse.from(domainService.get(id)));
    }

    @PostMapping
    public HttpEntity<DomainResponse> save(@RequestBody DomainCreateRequest request) {
        return ResponseEntity.ok(domainService.save(request));
    }

    @PutMapping("/{id}")
    public HttpEntity<DomainResponse> update(@PathVariable String id, @RequestBody DomainUpdateRequest request) {
        return ResponseEntity.ok(domainService.update(id, request));
    }
}
