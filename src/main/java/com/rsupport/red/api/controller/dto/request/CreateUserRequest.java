package com.rsupport.red.api.controller.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

@Schema(description = "사용자 생성 요청 DTO")
public record CreateUserRequest(
        @Schema(description = "생성할 사용자 username(email)", defaultValue = "red_1st_prize@rsupport.com")
                @Email
                @NotBlank(message = "사용자ID(email)")
                String username,
        @Schema(description = "사용자 이름", defaultValue = "team viewer") @NotBlank(message = "이름") String name,
        @Schema(description = "사용자 role code ", defaultValue = "2") @DecimalMin(value = "1") @DecimalMax(value = "2")
                Long role_code) {}
