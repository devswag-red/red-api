package com.rsupport.red.api.controller.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

@Schema(description = "사용자 인증 요청 DTO")
public record LoginRequest(
        @Schema(description = "사용자 ID(email)", defaultValue = "admin@rsupport.com")
                @Email
                @NotBlank(message = "사용자ID(email)")
                String username,
        @Schema(description = "사용자 비밀번호", defaultValue = "111111") @NotBlank(message = "비밀번호") String password) {}
