package com.rsupport.red.api.controller;

import com.rsupport.red.api.common.config.SwaggerConfig;
import com.rsupport.red.api.controller.dto.ClientCreateRequest;
import com.rsupport.red.api.controller.dto.ClientRequest;
import com.rsupport.red.api.controller.dto.ClientResponse;
import com.rsupport.red.api.controller.dto.ClientUpdateRequest;
import com.rsupport.red.api.service.ClientService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/client")
@RestController
@RequiredArgsConstructor
@SecurityRequirement(name = SwaggerConfig.SECURITY_SCHEME_KEY)
public class ClientController {

    private final ClientService clientService;

    @GetMapping
    public HttpEntity<List<ClientResponse>> content(ClientRequest request) {
        return ResponseEntity.ok(clientService.findAll(request));
    }

    @GetMapping("/{id}")
    public HttpEntity<ClientResponse> detail(@PathVariable String id) {
        return ResponseEntity.ok(ClientResponse.from(clientService.get(id)));
    }

    @PostMapping
    public HttpEntity<ClientResponse> save(@RequestBody ClientCreateRequest request) {
        return ResponseEntity.ok(clientService.save(request));
    }

    @PutMapping("/{id}")
    public HttpEntity<ClientResponse> update(@PathVariable String id, @RequestBody ClientUpdateRequest request) {
        return ResponseEntity.ok(clientService.update(id, request));
    }
}
