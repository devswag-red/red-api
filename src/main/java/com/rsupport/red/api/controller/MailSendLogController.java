package com.rsupport.red.api.controller;

import com.rsupport.red.api.common.config.SwaggerConfig;
import com.rsupport.red.api.controller.dto.MailSendLogRequest;
import com.rsupport.red.api.controller.dto.MailSendLogResponse;
import com.rsupport.red.api.service.MailSendLogService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/mail-log")
@RestController
@RequiredArgsConstructor
@SecurityRequirement(name = SwaggerConfig.SECURITY_SCHEME_KEY)
public class MailSendLogController {

    private final MailSendLogService mailSendLogService;

    @GetMapping
    public ResponseEntity<List<MailSendLogResponse>> getList(MailSendLogRequest request) {
        var list = mailSendLogService.getList(request).stream()
                .map(MailSendLogResponse::of)
                .toList();

        return ResponseEntity.ok(list);
    }
}
