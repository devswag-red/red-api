package com.rsupport.red.api.controller.dto;

import com.rsupport.red.api.domain.MailServiceProvider;
import com.rsupport.red.api.domain.eums.Provider;
import java.time.Instant;

public record MailServiceProviderResponse(
        String id,
        Provider provider,
        String username,
        String password,
        String host,
        String port,
        int priority,
        boolean enabled,
        String sender,
        Instant createdTime,
        int failCount,
        DomainResponse domain) {

    public static MailServiceProviderResponse of(MailServiceProvider mailServiceProvider) {
        return new MailServiceProviderResponse(
                mailServiceProvider.getId(),
                mailServiceProvider.getProvider(),
                mailServiceProvider.getUsername(),
                mailServiceProvider.getPassword(),
                mailServiceProvider.getHost(),
                mailServiceProvider.getPort(),
                mailServiceProvider.getPriority(),
                mailServiceProvider.isEnabled(),
                mailServiceProvider.getSender(),
                mailServiceProvider.getCreatedAt(),
                mailServiceProvider.getFailCount(),
                DomainResponse.from(mailServiceProvider.getDomain()));
    }
}
