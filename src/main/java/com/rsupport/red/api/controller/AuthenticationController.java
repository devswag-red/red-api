package com.rsupport.red.api.controller;

import com.rsupport.red.api.common.constant.UriConstants;
import com.rsupport.red.api.controller.dto.request.LoginRequest;
import com.rsupport.red.api.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "인증 처리", description = "사용자 인증 api")
@RestController
@RequiredArgsConstructor
@RequestMapping(UriConstants.AUTHENTICATE_ROOT)
public class AuthenticationController {

    private final UserService userService;

    @GetMapping
    public ResponseEntity<?> logIn(@Valid LoginRequest loginRequest) {
        return ResponseEntity.ok(userService.authenticate(loginRequest.username(), loginRequest.password()));
    }
}
