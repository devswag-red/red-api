package com.rsupport.red.api.controller.dto;

import java.time.Instant;

public record MailSendLogRequest(
        Instant startTime, Instant endTime, SearchTarget target, String keyword, SendStatus sendStatus) {

    public enum SearchTarget {
        SENDER,
        RECEIVER,
        DOMAIN_NAME
    }

    public enum SendStatus {
        ALL,
        SUCCESS,
        FAIL
    }
}
