package com.rsupport.red.api.controller.dto;

public record ClientCreateRequest(String domainId, boolean enabled) {}
