package com.rsupport.red.api.controller.dto;

import com.rsupport.red.api.domain.Client;
import com.rsupport.red.api.domain.Domain;
import java.time.Instant;

public record ClientResponse(
        String id, String domainId, String domainName, String secret, boolean enabled, Instant createdTime) {
    public static ClientResponse from(Client client) {
        Domain domain = client.getDomain();
        return new ClientResponse(
                client.getId(),
                domain.getId(),
                domain.getName(),
                client.getSecret(),
                client.isEnabled(),
                client.getCreatedAt());
    }
}
