package com.rsupport.red.api.controller.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(description = "사용자 삭제 요청 DTO")
public record DeleteUserRequest(
        @Schema(description = "삭제할 사용자 ID(email)", defaultValue = "user99") @NotBlank(message = "삭제할 사용자 ID")
                String targetId) {}
