package com.rsupport.red.api.controller.dto;

public record DomainRequest(SearchTarget target, String keyword) {

    public enum SearchTarget {
        ID,
        NAME
    }
}
