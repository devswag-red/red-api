package com.rsupport.red.api.controller;

import com.rsupport.red.api.common.exception.BaseException;
import com.rsupport.red.api.common.exception.ErrorCode;
import com.rsupport.red.api.common.mail.MessageDto;
import com.rsupport.red.api.common.mail.ProviderDto;
import com.rsupport.red.api.common.mail.RedMailSender;
import com.rsupport.red.api.common.mail.SendResultDto;
import com.rsupport.red.api.controller.dto.MailSendRequest;
import com.rsupport.red.api.domain.Client;
import com.rsupport.red.api.domain.MailSendLog;
import com.rsupport.red.api.domain.MailServiceProvider;
import com.rsupport.red.api.service.ClientService;
import com.rsupport.red.api.service.MailSendLogService;
import com.rsupport.red.api.service.MailServiceProviderService;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping("/mail-send")
@RestController
@RequiredArgsConstructor
public class MailSendController {

    private final ClientService clientService;
    private final RedMailSender mailSender;
    private final MailSendLogService mailSendLogService;
    private final MailServiceProviderService providerService;

    @PostMapping
    public ResponseEntity<Boolean> mailSend(@RequestBody MailSendRequest request) {
        Client client = clientService.getClient(request.secretKey());
        List<MailServiceProvider> providers =
                providerService.getEnabled(client.getDomain().getId());

        checkProvider(providers);

        boolean sent = send(request, providers);

        return ResponseEntity.ok(sent);
    }

    private void checkProvider(List<MailServiceProvider> providers) {
        if (providers.isEmpty()) {
            throw new BaseException(ErrorCode.EMPTY_ENABLED_MAIL_SERVICE_PROVIDER);
        }
    }

    private boolean send(MailSendRequest request, List<MailServiceProvider> providers) {
        for (MailServiceProvider provider : providers) {
            ProviderDto providerDto = createProviderDto(provider);
            MessageDto messageDto = request.toMessageDto(provider.getSender());

            SendResultDto sendResultDto = mailSender.send(providerDto, messageDto);
            afterSent(provider, messageDto, sendResultDto);

            if (!sendResultDto.shouldResend()) {
                return sendResultDto.success();
            }
        }

        return false;
    }

    private ProviderDto createProviderDto(MailServiceProvider provider) {
        return ProviderDto.create(
                provider.getHost(),
                Integer.parseInt(provider.getPort()), // NumberFormatException
                provider.getUsername(),
                provider.getPassword());
    }

    private void afterSent(MailServiceProvider provider, MessageDto messageDto, SendResultDto sendResultDto) {
        try {
            String requestId = UUID.randomUUID().toString();
            mailSendLogService.save(createMailSendLog(requestId, provider, messageDto, sendResultDto));

            if (sendResultDto.success()) {
                providerService.clearFailCount(provider.getId());
            } else if (sendResultDto.shouldResend()) {
                providerService.increaseFailCount(provider.getId());
            }
        } catch (Exception exception) {
            log.error("", exception);
        }
    }

    private MailSendLog createMailSendLog(
            String requestId, MailServiceProvider provider, MessageDto messageDto, SendResultDto sendResultDto) {
        return new MailSendLog(
                requestId,
                messageDto.sender(),
                messageDto.receiver(),
                sendResultDto.success(),
                provider.getDomain().getId(),
                provider.getDomain().getName(),
                provider.getId(),
                provider.getProvider().toString(),
                sendResultDto.getResultCode().getCode(),
                sendResultDto.getResultCode().getDescription());
    }
}
