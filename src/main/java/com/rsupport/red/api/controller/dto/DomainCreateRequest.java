package com.rsupport.red.api.controller.dto;

public record DomainCreateRequest(String name) {}
