package com.rsupport.red.api.controller;

import com.rsupport.red.api.common.config.SwaggerConfig;
import com.rsupport.red.api.controller.dto.MailServiceProviderListRequest;
import com.rsupport.red.api.controller.dto.MailServiceProviderRequest;
import com.rsupport.red.api.controller.dto.MailServiceProviderResponse;
import com.rsupport.red.api.service.MailServiceProviderService;
import com.rsupport.red.base.dto.IdResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/mail-service-provider")
@RestController
@RequiredArgsConstructor
@SecurityRequirement(name = SwaggerConfig.SECURITY_SCHEME_KEY)
public class MailServiceProviderController {

    private final MailServiceProviderService mailServiceProviderService;

    @PostMapping
    public ResponseEntity<IdResponse> save(@RequestBody MailServiceProviderRequest request) {
        var id = mailServiceProviderService.save(request);
        return ResponseEntity.ok(new IdResponse(id));
    }

    @GetMapping("/{id}")
    public ResponseEntity<MailServiceProviderResponse> getDetail(@PathVariable String id) {
        var response = mailServiceProviderService.getDetail(id);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable String id, @RequestBody MailServiceProviderRequest request) {
        mailServiceProviderService.update(id, request);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<List<MailServiceProviderResponse>> getList(MailServiceProviderListRequest request) {
        var response = mailServiceProviderService.getList(request);
        return ResponseEntity.ok(response);
    }
}
