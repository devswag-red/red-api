package com.rsupport.red.api.controller.dto;

import com.rsupport.red.api.domain.Domain;
import java.time.Instant;

public record DomainResponse(String id, String name, Instant createdTime) {

    public static DomainResponse from(Domain domain) {
        return new DomainResponse(domain.getId(), domain.getName(), domain.getCreatedAt());
    }
}
