package com.rsupport.red.api.controller.dto.response;

import com.rsupport.red.api.domain.User;
import java.time.Instant;
import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;

public record UserResponse(
        String id,
        String username,
        String name,
        Collection<? extends GrantedAuthority> authorities,
        Instant createdAt) {
    public static UserResponse of(User user) {
        return new UserResponse(
                user.getId(), user.getUsername(), user.getName(), user.getAuthorities(), user.getCreatedAt());
    }
}
