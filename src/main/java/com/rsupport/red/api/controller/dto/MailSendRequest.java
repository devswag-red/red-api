package com.rsupport.red.api.controller.dto;

import com.rsupport.red.api.common.mail.MessageDto;

public record MailSendRequest(String secretKey, String receiver, String subject, String text) {

    public MessageDto toMessageDto(String sender) {
        return MessageDto.create(sender, receiver, subject, text);
    }
}
