package com.rsupport.red.api.controller;

import com.rsupport.red.api.common.config.SwaggerConfig;
import com.rsupport.red.api.common.constant.UriConstants;
import com.rsupport.red.api.common.exception.BaseException;
import com.rsupport.red.api.common.exception.ErrorCode;
import com.rsupport.red.api.controller.dto.request.CreateUserRequest;
import com.rsupport.red.api.controller.dto.request.DeleteUserRequest;
import com.rsupport.red.api.controller.dto.request.UpdatePasswordRequest;
import com.rsupport.red.api.controller.dto.request.UpdateUserRequest;
import com.rsupport.red.api.domain.User;
import com.rsupport.red.api.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(UriConstants.USER_ROOT)
@Tag(name = "사용자 관리", description = "사용자 관리 api \n 인증후 받은 토큰 HttpHeaders.AUTHORIZATION 으로 헤더에 필수")
@SecurityRequirement(name = SwaggerConfig.SECURITY_SCHEME_KEY)
public class UserController {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    private final String DEFAULT_USER_PASSWORD = "111111";

    @Operation(summary = "사용자 정보 조회", description = "사용자 정보 조회")
    @ApiResponse(responseCode = "200", description = "성공")
    @GetMapping("/{id}")
    public ResponseEntity<?> getUserDetail(@PathVariable String id) {
        return ResponseEntity.ok(userService.getUser(id));
    }

    @Operation(
            summary = "사용자 목록 조회",
            description = "목록 조회\n\nexample param(not required)\n\n"
                    + "{ \"page\": 1,  \"size\": 1, \"sort\": \"id\" }\n\n"
                    + "page : 페이지번호(1~), size : 페이지사이즈 sort : 정렬 ")
    @ApiResponse(responseCode = "200", description = "성공")
    @GetMapping
    public ResponseEntity<?> getUserList(
            @PageableDefault(size = 100, sort = "id") @Parameter(hidden = true) Pageable pageable) {
        return ResponseEntity.ok().body(userService.getUserList(pageable));
    }

    @Operation(summary = "사용자 생성", description = "사용자 생성")
    @ApiResponse(responseCode = "200", description = "성공")
    @PostMapping
    public ResponseEntity<?> createNewUser(@RequestBody @Valid CreateUserRequest createUserRequest) {
        if (!getLoggedInUser().isAdmin()) {
            throw new BaseException(ErrorCode.PERMISSION_DENIED);
        }
        checkExistUsername(createUserRequest.username());
        User newUser = User.of(
                createUserRequest.username(),
                createUserRequest.name(),
                passwordEncoder.encode(DEFAULT_USER_PASSWORD),
                createUserRequest.role_code());
        return ResponseEntity.ok(userService.saveUser(newUser));
    }

    @Operation(summary = "사용자 수정", description = "사용자 수정")
    @ApiResponse(responseCode = "200", description = "성공")
    @PutMapping("/{id}")
    public ResponseEntity<?> updateUser(
            @PathVariable String id, @RequestBody @Valid UpdateUserRequest updateUserRequest) {
        checkAdminOrSelf(id);
        return ResponseEntity.ok(userService.updateUser(id, updateUserRequest));
    }

    @Operation(summary = "비밀번호 수정", description = "비밀번호 수정")
    @ApiResponse(responseCode = "200", description = "성공")
    @PutMapping(UriConstants.USER_PASSWORD)
    public ResponseEntity<?> updatePassword(@RequestBody @Valid UpdatePasswordRequest updatePasswordRequest) {
        checkAdminOrSelf(updatePasswordRequest.id());
        return ResponseEntity.ok(userService.updatePassword(updatePasswordRequest));
    }

    @Operation(summary = "사용자 삭제", description = "사용자 삭제")
    @ApiResponse(responseCode = "200", description = "성공")
    @DeleteMapping
    public ResponseEntity<?> deleteUser(@RequestBody @Valid DeleteUserRequest deleteUserRequest) {
        if (!getLoggedInUser().isAdmin()) {
            throw new BaseException(ErrorCode.PERMISSION_DENIED);
        }
        userService.deleteUser(deleteUserRequest);
        return ResponseEntity.ok(null);
    }

    private void checkExistUsername(String username) {
        boolean userExists = userService.checkExistUsername(username);
        if (userExists) {
            throw new BaseException(ErrorCode.DUPLICATE_USERNAME);
        }
    }

    private void checkAdminOrSelf(String userId) {
        User loggedInUser = getLoggedInUser();
        User user = userService.get(userId);
        if (!loggedInUser.isAdmin() && !loggedInUser.equals(user)) {
            throw new BaseException(ErrorCode.PERMISSION_DENIED);
        }
    }

    private User getLoggedInUser() {
        String userId = SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal()
                .toString();
        return userService.get(userId);
    }
}
