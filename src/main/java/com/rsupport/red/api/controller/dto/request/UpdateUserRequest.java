package com.rsupport.red.api.controller.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

@Schema(description = "사용자 비밀번호 변경 요청 DTO")
public record UpdateUserRequest(
        @Schema(description = "변경할 사용자 ID", defaultValue = "user02") @Email @NotBlank(message = "사용자ID(email)")
                String username,
        @Schema(description = "변경할 사용자 이름", defaultValue = "mail_sender") @NotBlank(message = "사용자 이름") String name,
        @Schema(description = "변경할 role", defaultValue = "2") @DecimalMin(value = "1") @DecimalMax(value = "2")
                Long role_code) {}
