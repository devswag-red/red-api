package com.rsupport.red.api.repository;

import com.rsupport.red.api.domain.MailSendLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MailSendLogRepository extends JpaRepository<MailSendLog, String>, MailSendLogRepositoryCustom {}
