package com.rsupport.red.api.repository;

import com.rsupport.red.api.domain.MailServiceProvider;
import jakarta.persistence.LockModeType;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

public interface MailServiceProviderRepository extends JpaRepository<MailServiceProvider, String> {

    @Override
    @EntityGraph(attributePaths = "domain")
    List<MailServiceProvider> findAll();

    @EntityGraph(attributePaths = "domain")
    List<MailServiceProvider> findByDomainIdAndEnabledTrueOrderByPriority(String domainId);

    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    Optional<MailServiceProvider> findForUpdateById(String id);
}
