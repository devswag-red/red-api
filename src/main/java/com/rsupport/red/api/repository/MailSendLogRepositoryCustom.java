package com.rsupport.red.api.repository;

import com.rsupport.red.api.controller.dto.MailSendLogRequest;
import com.rsupport.red.api.domain.MailSendLog;
import java.util.List;

public interface MailSendLogRepositoryCustom {
    List<MailSendLog> getList(MailSendLogRequest request);
}
