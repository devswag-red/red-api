package com.rsupport.red.api.repository;

import com.rsupport.red.api.domain.Client;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, String> {

    @Override
    @EntityGraph(attributePaths = "domain")
    List<Client> findAll();

    @EntityGraph(attributePaths = "domain")
    Optional<Client> findBySecret(String secret);

    @Override
    @EntityGraph(attributePaths = "domain")
    Optional<Client> findById(String id);
}
