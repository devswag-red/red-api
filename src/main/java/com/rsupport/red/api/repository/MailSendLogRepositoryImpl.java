package com.rsupport.red.api.repository;

import static com.rsupport.red.api.domain.QMailSendLog.mailSendLog;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.rsupport.red.api.controller.dto.MailSendLogRequest;
import com.rsupport.red.api.controller.dto.MailSendLogRequest.SearchTarget;
import com.rsupport.red.api.controller.dto.MailSendLogRequest.SendStatus;
import com.rsupport.red.api.domain.MailSendLog;
import java.time.Instant;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@RequiredArgsConstructor
public class MailSendLogRepositoryImpl implements MailSendLogRepositoryCustom {

    private final JPAQueryFactory queryFactory;

    @Override
    public List<MailSendLog> getList(MailSendLogRequest request) {
        // TODO: 페이징 처리
        return queryFactory
                .selectFrom(mailSendLog)
                .where(
                        dateCondition(request.startTime(), request.endTime()),
                        sendStatusCondition(request.sendStatus()),
                        keywordCondition(request.target(), request.keyword()))
                .orderBy(mailSendLog.createdAt.desc())
                .fetch();
    }

    private BooleanExpression dateCondition(Instant startTime, Instant endTime) {
        return mailSendLog.createdAt.between(startTime, endTime);
    }

    private BooleanExpression sendStatusCondition(SendStatus sendStatus) {
        return switch (sendStatus) {
            case ALL -> null;
            case SUCCESS -> mailSendLog.success.eq(true);
            case FAIL -> mailSendLog.success.eq(false);
        };
    }

    private BooleanExpression keywordCondition(SearchTarget target, String keyword) {
        if (StringUtils.isBlank(keyword)) {
            return null;
        }

        return switch (target) {
            case SENDER -> mailSendLog.sender.contains(keyword);
            case RECEIVER -> mailSendLog.receiver.contains(keyword);
            case DOMAIN_NAME -> mailSendLog.domainName.eq(keyword);
        };
    }
}
