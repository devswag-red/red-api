package com.rsupport.red.api.repository;

import com.rsupport.red.api.domain.Domain;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DomainRepository extends JpaRepository<Domain, String> {}
