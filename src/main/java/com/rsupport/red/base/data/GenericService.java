package com.rsupport.red.base.data;

import com.rsupport.red.api.common.exception.BaseException;
import com.rsupport.red.api.common.exception.ErrorCode;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.util.TypeInformation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public abstract class GenericService<T, ID, R extends JpaRepository<T, ID>> {

    protected R repository;

    @Autowired
    public void setRepository(ApplicationContext applicationContext) {
        this.repository = applicationContext.getBean(getRepositoryClass());
    }

    @SuppressWarnings("unchecked")
    private Class<R> getRepositoryClass() {
        List<TypeInformation<?>> arguments = TypeInformation.of(this.getClass())
                .getRequiredSuperTypeInformation(GenericService.class)
                .getTypeArguments();

        return (Class<R>) arguments.get(2).getType();
    }

    public List<T> findAll() {
        return repository.findAll();
    }

    public T get(ID id) {
        return repository.findById(id).orElseThrow(() -> new BaseException(ErrorCode.DATA_NOT_FOUND));
    }

    public T find(ID id) {
        return repository.findById(id).orElse(null);
    }

    public boolean exists(ID id) {
        return repository.existsById(id);
    }

    @Transactional
    public T save(T object) {
        return repository.save(object);
    }

    @Transactional
    public void deleteById(ID id) {
        repository.deleteById(id);
    }

    @Transactional
    public void delete(T entity) {
        repository.delete(entity);
    }
}
