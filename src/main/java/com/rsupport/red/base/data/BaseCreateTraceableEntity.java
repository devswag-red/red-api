package com.rsupport.red.base.data;

import com.rsupport.red.api.common.constant.ColumnSize;
import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Getter
public abstract class BaseCreateTraceableEntity extends BaseCreateEntity {

    @Column(updatable = false, length = ColumnSize.UUID)
    @CreatedBy
    protected String createdBy;
}
