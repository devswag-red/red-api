package com.rsupport.red.base.data;

import com.rsupport.red.api.common.constant.ColumnSize;
import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import java.time.Instant;
import lombok.Getter;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Getter
public abstract class BaseTraceableEntity extends BaseCreateEntity {

    @LastModifiedDate
    protected Instant updatedAt;

    @Column(length = ColumnSize.UUID)
    @LastModifiedBy
    protected String updatedBy;
}
