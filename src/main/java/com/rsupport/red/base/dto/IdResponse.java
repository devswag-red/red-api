package com.rsupport.red.base.dto;

public record IdResponse(String id) {}
