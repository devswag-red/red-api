-- user
insert into rt_user(id, username, name, password, role_code, created_at) values ('user01', 'admin@rsupport.com', 'Rsupport','{bcrypt}$2a$10$OhB44QvRyCP5Cnb7Tt870ePRB9cFDOGGZjviGkbZcl1JuR5uiKBZu', 'ADMIN', '2000-01-01 15:00:00');
insert into rt_user(id, username, name, password, role_code, created_at) values ('user02', 'admin2222@rsupport.com', 'Rsupport','{bcrypt}$2a$10$OhB44QvRyCP5Cnb7Tt870ePRB9cFDOGGZjviGkbZcl1JuR5uiKBZu', 'ADMIN', '2000-01-01 15:00:00');
insert into rt_user(id, username, name, password, role_code, created_at) values ('user99', 'user99@rsupport.com', 'Rsupport','{bcrypt}$2a$10$OhB44QvRyCP5Cnb7Tt870ePRB9cFDOGGZjviGkbZcl1JuR5uiKBZu', 'USER', '2000-01-01 15:00:00');

-- domain
insert into rt_domain(id, name, created_at) values ('domain01', 'RV', '2024-08-01 15:00:00');
insert into rt_domain(id, name, created_at) values ('domain02', 'RC', '2024-08-02 15:00:00');
insert into rt_domain(id, name, created_at) values ('domain03', 'RM', '2024-08-03 15:00:00');

-- client
insert into rt_client(id, domain_id, secret, enabled, created_at) values ('client01', 'domain01', 'secret01', 1, '2024-08-15 15:00:00');
insert into rt_client(id, domain_id, secret, enabled, created_at) values ('client02', 'domain01', 'secret02', 0, '2024-08-15 15:00:00');
insert into rt_client(id, domain_id, secret, enabled, created_at) values ('client03', 'domain02', 'secret03', 1, '2024-08-15 15:00:00');
insert into rt_client(id, domain_id, secret, enabled, created_at) values ('client04', 'domain03', 'secret04', 1, '2024-08-15 15:00:00');

-- mail-log
INSERT INTO lt_mail_send (id, created_at, domain_id, domain_name, fail_code, fail_message, provider_id, provider_type, receiver, request_id, sender, success) VALUES('mail-send-log-01', '2024-09-01 15:00:00.000', 'domain01', 'RV', '300', 'Authentication failed', 'RV-AWS', 'AWS', 'receiver01@rsupport.com', 'a54c40dd-3d79-41ef-a079-f8d1e7ec0dcd', 'sender01@rsupport.com', 0);
INSERT INTO lt_mail_send (id, created_at, domain_id, domain_name, fail_code, fail_message, provider_id, provider_type, receiver, request_id, sender, success) VALUES('mail-send-log-02', '2024-09-01 15:00:00.000', 'domain01', 'RV', NULL, NULL, 'RV-OCI', 'OCI', 'receiver01@rsupport.com', 'a54c40dd-3d79-41ef-a079-f8d1e7ec0dcd', 'sender01@rsupport.com', 1);
INSERT INTO lt_mail_send (id, created_at, domain_id, domain_name, fail_code, fail_message, provider_id, provider_type, receiver, request_id, sender, success) VALUES('mail-send-log-03', '2024-09-01 15:00:00.000', 'domain02', 'RC', NULL, NULL, 'RC-AWS', 'AWS', 'receiver02@rsupport.com', '35d4damc-3d79-41ef-a079-f8d1e7ec0dcd', 'sender02@rsupport.com', 1);
INSERT INTO lt_mail_send (id, created_at, domain_id, domain_name, fail_code, fail_message, provider_id, provider_type, receiver, request_id, sender, success) VALUES('mail-send-log-04', '2024-09-02 15:00:00.000', 'domain02', 'RC', NULL, NULL, 'RC-AWS', 'AWS', 'receiver03@rsupport.com', 'dm2gf893-3d79-41ef-a079-f8d1e7ec0dcd', 'sender03@rsupport.com', 1);
INSERT INTO lt_mail_send (id, created_at, domain_id, domain_name, fail_code, fail_message, provider_id, provider_type, receiver, request_id, sender, success) VALUES('mail-send-log-05', '2024-09-02 15:00:00.000', 'domain02', 'RC', '400', 'Receiving failed', 'RV-OCI', 'OCI', 'receiver03@rsupport.com', 'dm2gf893-3d79-41ef-a079-f8d1e7ec0dcd', 'sender03@rsupport.com', 0);
INSERT INTO lt_mail_send (id, created_at, domain_id, domain_name, fail_code, fail_message, provider_id, provider_type, receiver, request_id, sender, success) VALUES('mail-send-log-06', '2024-09-03 15:00:00.000', 'domain02', 'RC', NULL, NULL, 'RV-OCI', 'OCI', 'receiver04@rsupport.com', 'd92kdnbe-3d79-41ef-a079-f8d1e7ec0dcd', 'sender04@rsupport.com', 1);
INSERT INTO lt_mail_send (id, created_at, domain_id, domain_name, fail_code, fail_message, provider_id, provider_type, receiver, request_id, sender, success) VALUES('mail-send-log-07', '2024-09-01 15:00:00.000', 'domain03', 'RM', NULL, NULL, 'RM-AWS', 'AWS', 'receiver05@rsupport.com', '1hdb883n-3d79-41ef-a079-f8d1e7ec0dcd', 'sender05@rsupport.com', 1);
INSERT INTO lt_mail_send (id, created_at, domain_id, domain_name, fail_code, fail_message, provider_id, provider_type, receiver, request_id, sender, success) VALUES('mail-send-log-08', '2024-09-02 15:00:00.000', 'domain03', 'RM', NULL, NULL, 'RM-AWS', 'AWS', 'receiver06@rsupport.com', '9dh37fap-3d79-41ef-a079-f8d1e7ec0dcd', 'sender06@rsupport.com', 1);
INSERT INTO lt_mail_send (id, created_at, domain_id, domain_name, fail_code, fail_message, provider_id, provider_type, receiver, request_id, sender, success) VALUES('mail-send-log-09', '2024-09-01 15:00:00.000', 'domain03', 'RM', NULL, NULL, 'RM-OCI', 'OCI', 'receiver07@rsupport.com', 'dd63bbs8-3d79-41ef-a079-f8d1e7ec0dcd', 'sender07@rsupport.com', 1);
INSERT INTO lt_mail_send (id, created_at, domain_id, domain_name, fail_code, fail_message, provider_id, provider_type, receiver, request_id, sender, success) VALUES('mail-send-log-10', '2024-09-03 15:00:00.000', 'domain03', 'RM', NULL, NULL, 'RM-OCI', 'OCI', 'receiver08@rsupport.com', '227dcd5a-3d79-41ef-a079-f8d1e7ec0dcd', 'sender08@rsupport.com', 1);

-- mail-service-provider
insert into rt_mail_service_provider (id, provider, username, password, host, port, priority, enabled, sender, domain_id, created_at) values ('mail-service-provider01', 'AWS', 'username', 'password', 'host', 'port', 1, true, 'sender', 'domain02', '2024-09-11 11:00:00');
