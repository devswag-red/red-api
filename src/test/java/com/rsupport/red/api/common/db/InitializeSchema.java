package com.rsupport.red.api.common.db;

import static org.assertj.core.api.Fail.fail;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestClassOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;

@TestClassOrder(ClassOrderer.OrderAnnotation.class)
@Slf4j
class InitializeSchema {

    private static final String DB_NAME_KEY = "db.name";
    private static final String DB_NAME = System.getProperty(DB_NAME_KEY);

    @BeforeAll
    static void beforeAll() {
        checkRequiredSystemProperty();
        printRequiredSystemProperty();
    }

    private static void checkRequiredSystemProperty() {
        if (DB_NAME == null || DB_NAME.isBlank()) {
            log.debug(System.getProperty(DB_NAME_KEY));
            fail(String.format("'%s' must not be blank", DB_NAME_KEY));
        }
    }

    private static void printRequiredSystemProperty() {
        log.info("{}: {}", DB_NAME_KEY, DB_NAME);
    }

    @Nested
    @Order(1)
    @SpringBootTest
    class Drop {

        @Autowired
        private JdbcTemplate jdbcTemplate;

        @Test
        void drop() {
            log.info("drop schema. dbName : {}", DB_NAME);
            jdbcTemplate.execute("drop database if exists " + DB_NAME);
        }
    }

    @Nested
    @Order(2)
    @SpringBootTest
    @ActiveProfiles("init")
    class Create {

        @Test
        void create() {
            log.info("create schema");
        }
    }
}
