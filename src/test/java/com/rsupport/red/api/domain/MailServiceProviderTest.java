package com.rsupport.red.api.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.rsupport.red.api.domain.eums.Provider;
import org.junit.jupiter.api.Test;

class MailServiceProviderTest {

    @Test
    void updateEnabledByIncreaseFailCount() {
        // given
        var mailServiceProvider = create();

        // when
        mailServiceProvider.increaseFailCount();
        mailServiceProvider.increaseFailCount();
        mailServiceProvider.increaseFailCount();
        mailServiceProvider.increaseFailCount();
        mailServiceProvider.increaseFailCount();
        mailServiceProvider.increaseFailCount();

        // then
        assertThat(mailServiceProvider.isEnabled()).isFalse();
    }

    private MailServiceProvider create() {
        return MailServiceProvider.of(
                Provider.AWS, "username", "password", "host", "80", 1, true, "sender", Domain.from("domain"));
    }
}
