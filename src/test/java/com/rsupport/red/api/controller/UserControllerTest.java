package com.rsupport.red.api.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.rsupport.red.api.common.constant.UriConstants;
import com.rsupport.red.api.common.exception.ErrorCode;
import com.rsupport.red.api.controller.dto.request.CreateUserRequest;
import com.rsupport.red.api.controller.dto.request.DeleteUserRequest;
import com.rsupport.red.api.controller.dto.request.UpdateUserRequest;
import com.rsupport.red.base.AbstractIntegrationTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Transactional
@DisplayName(value = "User Controller Test")
class UserControllerTest extends AbstractIntegrationTest {

    @Test
    void testGetUserDetail() throws Exception {
        mockMvc.perform(get(UriConstants.USER_ROOT + "/user01").header(HttpHeaders.AUTHORIZATION, testAdminToken))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testGetUserList() throws Exception {
        mockMvc.perform(get(UriConstants.USER_ROOT)
                        .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                        .param("page", "0")
                        .param("size", "100")
                        .param("sort", "id"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testUpdateUser() throws Exception {
        var request = new UpdateUserRequest("testUser12123@rsupport.com", "updated_user", 2L);
        mockMvc.perform(put(UriConstants.USER_ROOT + "/user01")
                        .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJsonString(request)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testCreateUser() throws Exception {

        var request = new CreateUserRequest("testUser11111@test.com", "test_user", 2L);
        mockMvc.perform(post(UriConstants.USER_ROOT)
                        .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJsonString(request)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testCreateUser_fail() throws Exception {

        var request = new CreateUserRequest("admin@rsupport.com", "test_user", 2L);
        mockMvc.perform(post(UriConstants.USER_ROOT)
                        .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJsonString(request)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode").value(ErrorCode.DUPLICATE_USERNAME.getCode()));
    }

    @Test
    void testDeleteUser() throws Exception {
        var request = new DeleteUserRequest("user99");
        mockMvc.perform(delete(UriConstants.USER_ROOT)
                        .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJsonString(request)))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
