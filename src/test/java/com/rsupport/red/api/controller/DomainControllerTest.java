package com.rsupport.red.api.controller;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.rsupport.red.api.controller.dto.DomainCreateRequest;
import com.rsupport.red.api.controller.dto.DomainRequest;
import com.rsupport.red.api.controller.dto.DomainUpdateRequest;
import com.rsupport.red.base.AbstractIntegrationTest;
import com.rsupport.red.base.Fixtures.Domains;
import java.nio.charset.StandardCharsets;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

class DomainControllerTest extends AbstractIntegrationTest {

    @Test
    void content() throws Exception {
        var request = new DomainRequest(null, null);

        var result = mockMvc.perform(get("/domain")
                .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                .characterEncoding(StandardCharsets.UTF_8)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)));

        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$.*.name").value(hasItems("RV", "RC", "RM")));
        result.andExpect(jsonPath("$..['id']").exists());
        result.andExpect(jsonPath("$..['name']").exists());
        result.andExpect(jsonPath("$..['createdTime']").exists());
    }

    @Test
    void detail() throws Exception {
        var result = mockMvc.perform(get("/domain/" + Domains.Id.RV)
                .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                .characterEncoding(StandardCharsets.UTF_8)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$.id").isNotEmpty());
        result.andExpect(jsonPath("$.name").isNotEmpty());
    }

    @Test
    void save() throws Exception {
        var request = new DomainCreateRequest("new_domain");

        var result = mockMvc.perform(post("/domain")
                .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                .characterEncoding(StandardCharsets.UTF_8)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)));

        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$.id").isNotEmpty());
        result.andExpect(jsonPath("$.name").value("new_domain"));
    }

    @Test
    void update() throws Exception {
        var request = new DomainUpdateRequest("RV_SAAS");

        var result = mockMvc.perform(put("/domain/" + Domains.Id.RV)
                .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                .characterEncoding(StandardCharsets.UTF_8)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)));

        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$.id").value(Domains.Id.RV));
        result.andExpect(jsonPath("$.name").value("RV_SAAS"));
    }
}
