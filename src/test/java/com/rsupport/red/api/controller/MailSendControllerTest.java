package com.rsupport.red.api.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.rsupport.red.api.common.mail.RedMailSender;
import com.rsupport.red.api.common.mail.SendResultDto;
import com.rsupport.red.api.controller.dto.MailSendRequest;
import com.rsupport.red.api.domain.MailServiceProvider;
import com.rsupport.red.api.domain.eums.Provider;
import com.rsupport.red.api.repository.DomainRepository;
import com.rsupport.red.api.repository.MailServiceProviderRepository;
import com.rsupport.red.base.AbstractIntegrationTest;
import com.rsupport.red.base.Fixtures.Clients.Secret;
import com.rsupport.red.base.Fixtures.Domains;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.JsonFieldType;

@AutoConfigureRestDocs
class MailSendControllerTest extends AbstractIntegrationTest {

    @MockBean
    RedMailSender redMailSender;

    @Autowired
    private DomainRepository domainRepository;

    @Autowired
    private MailServiceProviderRepository mailServiceProviderRepository;

    @Test
    void send() throws Exception {
        // given
        when(redMailSender.send(any(), any())).thenReturn(SendResultDto.succeeded());

        Resource resource = new ClassPathResource("mail/templates/test-mail-template.html");
        String templateContents = Files.readString(resource.getFile().toPath());

        createMailServiceProvider();
        var request = new MailSendRequest(
                Secret.RM, "mail-send-test@rsupport.com", "[공지] DevSwag 일정 관련 공지", templateContents);

        // when
        var result = mockMvc.perform(post("/mail-send")
                .characterEncoding(StandardCharsets.UTF_8)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)));

        // then
        result.andExpect(status().isOk());

        result.andDo(document(
                "mail-send",
                preprocessRequest(prettyPrint()),
                requestFields(
                        fieldWithPath("secretKey").type(JsonFieldType.STRING).description("클라이언트 시크릿 키"),
                        fieldWithPath("receiver").type(JsonFieldType.STRING).description("받는이"),
                        fieldWithPath("subject").type(JsonFieldType.STRING).description("제목"),
                        fieldWithPath("text").type(JsonFieldType.STRING).description("내용"))));

        verify(redMailSender, times(1)).send(any(), any());
    }

    private MailServiceProvider createMailServiceProvider() {
        var domain = domainRepository.getReferenceById(Domains.Id.RM);
        return mailServiceProviderRepository.save(
                MailServiceProvider.of(Provider.AWS, "username", "password", "host", "80", 1, true, "rsup.io", domain));
    }
}
