package com.rsupport.red.api.controller;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.rsupport.red.api.controller.dto.ClientCreateRequest;
import com.rsupport.red.api.controller.dto.ClientRequest;
import com.rsupport.red.api.controller.dto.ClientUpdateRequest;
import com.rsupport.red.base.AbstractIntegrationTest;
import com.rsupport.red.base.Fixtures.Clients;
import com.rsupport.red.base.Fixtures.Domains;
import java.nio.charset.StandardCharsets;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

class ClientControllerTest extends AbstractIntegrationTest {

    @Test
    void content() throws Exception {
        var request = new ClientRequest(null, null);

        var result = mockMvc.perform(get("/client")
                .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                .characterEncoding(StandardCharsets.UTF_8)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)));

        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$..['id']").isNotEmpty());
        result.andExpect(jsonPath("$..['domainId']").isNotEmpty());
        result.andExpect(jsonPath("$.*.domainName").value(hasItems(Domains.Name.RV, Domains.Name.RC, Domains.Name.RM)));
        result.andExpect(jsonPath("$..['secret']").isNotEmpty());
        result.andExpect(jsonPath("$..['createdTime']").isNotEmpty());
    }

    @Test
    void detail() throws Exception {
        var result = mockMvc.perform(get("/client/client01")
                .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                .characterEncoding(StandardCharsets.UTF_8)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON));

        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$.id").isNotEmpty());
        result.andExpect(jsonPath("$.domainId").isNotEmpty());
        result.andExpect(jsonPath("$.domainName").isNotEmpty());
        result.andExpect(jsonPath("$.enabled").isNotEmpty());
    }

    @Test
    void save() throws Exception {
        var request = new ClientCreateRequest(Domains.Id.RV, true);

        var result = mockMvc.perform(post("/client")
                .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                .characterEncoding(StandardCharsets.UTF_8)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)));

        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$.id").isNotEmpty());
        result.andExpect(jsonPath("$.domainId").value(Domains.Id.RV));
        result.andExpect(jsonPath("$.domainName").value(Domains.Name.RV));
        result.andExpect(jsonPath("$.enabled").value(true));
    }

    @Test
    void update() throws Exception {
        var request = new ClientUpdateRequest(Domains.Id.RV, false);

        var result = mockMvc.perform(put("/client/" + Clients.Id.RV1)
                .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                .characterEncoding(StandardCharsets.UTF_8)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)));

        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$.id").value(Clients.Id.RV1));
        result.andExpect(jsonPath("$.domainId").value(Domains.Id.RV));
        result.andExpect(jsonPath("$.domainName").value(Domains.Name.RV));
        result.andExpect(jsonPath("$.secret").value(Clients.Secret.RV1));
        result.andExpect(jsonPath("$.enabled").value(false));
    }
}
