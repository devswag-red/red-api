package com.rsupport.red.api.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.rsupport.red.api.controller.dto.MailServiceProviderListRequest;
import com.rsupport.red.api.controller.dto.MailServiceProviderRequest;
import com.rsupport.red.api.domain.MailServiceProvider;
import com.rsupport.red.api.domain.eums.Provider;
import com.rsupport.red.api.repository.DomainRepository;
import com.rsupport.red.api.repository.MailServiceProviderRepository;
import com.rsupport.red.base.AbstractIntegrationTest;
import com.rsupport.red.base.Fixtures;
import com.rsupport.red.base.Fixtures.Domains;
import java.nio.charset.StandardCharsets;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

class MailServiceProviderControllerTest extends AbstractIntegrationTest {

    @Autowired
    private MailServiceProviderRepository mailServiceProviderRepository;

    @Autowired
    private DomainRepository domainRepository;

    @Test
    void save() throws Exception {
        // given
        var domainId = Domains.Id.RV;
        var request = new MailServiceProviderRequest(
                domainId, Provider.AWS, "username", "password", "host", "80", 1, true, "rsup.io");

        // when
        var result = mockMvc.perform(post("/mail-service-provider")
                .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                .characterEncoding(StandardCharsets.UTF_8)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)));

        // then
        result.andExpect(status().isOk());
    }

    @Test
    void getDetail() throws Exception {
        // given
        var mailServiceProvider = mailServiceProviderRepository.getReferenceById(Fixtures.MailServiceProviders.Id.TEST);

        // when
        var result = mockMvc.perform(get("/mail-service-provider/{id}", mailServiceProvider.getId())
                .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                .characterEncoding(StandardCharsets.UTF_8)
                .accept(MediaType.APPLICATION_JSON));

        // then
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$.id").isNotEmpty());
        result.andExpect(jsonPath("$.provider").isNotEmpty());
        result.andExpect(jsonPath("$.username").isNotEmpty());
        result.andExpect(jsonPath("$.password").isNotEmpty());
        result.andExpect(jsonPath("$.host").isNotEmpty());
        result.andExpect(jsonPath("$.port").isNotEmpty());
        result.andExpect(jsonPath("$.priority").isNotEmpty());
        result.andExpect(jsonPath("$.enabled").isNotEmpty());
        result.andExpect(jsonPath("$.domain").isNotEmpty());
    }

    @Test
    void update() throws Exception {
        // given
        var mailServiceProvider = mailServiceProviderRepository.getReferenceById(Fixtures.MailServiceProviders.Id.TEST);
        var request = new MailServiceProviderRequest(
                Domains.Id.RC,
                Provider.AWS,
                "updated-username",
                "updated-password",
                "updated-host",
                "8080",
                2,
                false,
                "rsup.io");

        // when
        var result = mockMvc.perform(put("/mail-service-provider/{id}", mailServiceProvider.getId())
                .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                .characterEncoding(StandardCharsets.UTF_8)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)));

        // then
        result.andExpect(status().isOk());

        var updatedMailServiceProvider = mailServiceProviderRepository
                .findById(mailServiceProvider.getId())
                .orElseThrow();
        assertAll(
                () -> assertThat(updatedMailServiceProvider.getProvider()).isEqualTo(request.provider()),
                () -> assertThat(updatedMailServiceProvider.getUsername()).isEqualTo(request.username()),
                () -> assertThat(updatedMailServiceProvider.getPassword()).isEqualTo(request.password()),
                () -> assertThat(updatedMailServiceProvider.getHost()).isEqualTo(request.host()),
                () -> assertThat(updatedMailServiceProvider.getPort()).isEqualTo(request.port()),
                () -> assertThat(updatedMailServiceProvider.getPriority()).isEqualTo(request.priority()),
                () -> assertThat(updatedMailServiceProvider.isEnabled()).isEqualTo(request.enabled()),
                () -> assertThat(updatedMailServiceProvider.getDomain().getId()).isEqualTo(request.domainId()));
    }

    private MailServiceProvider create() {
        var domain = domainRepository.getReferenceById(Domains.Id.RV);
        return mailServiceProviderRepository.save(
                MailServiceProvider.of(Provider.AWS, "username", "password", "host", "80", 1, true, "rsup.io", domain));
    }

    @Test
    void getListById() throws Exception {
        // given
        var mailServiceProvider = mailServiceProviderRepository.getReferenceById(Fixtures.MailServiceProviders.Id.TEST);
        var request = new MailServiceProviderListRequest(
                MailServiceProviderListRequest.SearchTarget.ID, mailServiceProvider.getId());

        // when
        var result = mockMvc.perform(get("/mail-service-provider")
                .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                .characterEncoding(StandardCharsets.UTF_8)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)));

        // then
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    void getListByProvider() throws Exception {
        // given
        var mailServiceProvider = mailServiceProviderRepository.getReferenceById(Fixtures.MailServiceProviders.Id.TEST);
        var request = new MailServiceProviderListRequest(
                MailServiceProviderListRequest.SearchTarget.PROVIDER,
                mailServiceProvider.getProvider().name());

        // when
        var result = mockMvc.perform(get("/mail-service-provider")
                .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                .characterEncoding(StandardCharsets.UTF_8)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)));

        // then
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    void getListByUsername() throws Exception {
        // given
        var mailServiceProvider = mailServiceProviderRepository.getReferenceById(Fixtures.MailServiceProviders.Id.TEST);
        var request = new MailServiceProviderListRequest(
                MailServiceProviderListRequest.SearchTarget.USERNAME, mailServiceProvider.getUsername());

        // when
        var result = mockMvc.perform(get("/mail-service-provider")
                .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                .characterEncoding(StandardCharsets.UTF_8)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)));

        // then
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    void getListByDomain() throws Exception {
        // given
        var mailServiceProvider = mailServiceProviderRepository.getReferenceById(Fixtures.MailServiceProviders.Id.TEST);
        var request = new MailServiceProviderListRequest(
                MailServiceProviderListRequest.SearchTarget.DOMAIN,
                mailServiceProvider.getDomain().getName());

        // when
        var result = mockMvc.perform(get("/mail-service-provider")
                .header(HttpHeaders.AUTHORIZATION, testAdminToken)
                .characterEncoding(StandardCharsets.UTF_8)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJsonString(request)));

        // then
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$").isNotEmpty());
    }
}
