package com.rsupport.red.api.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import com.rsupport.red.base.Fixtures.Clients;
import com.rsupport.red.base.RepositoryTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

@RepositoryTest
class ClientRepositoryTest {

    @Autowired
    private ClientRepository repository;

    @Test
    void findAll() {
        assertThatCode(repository::findAll).doesNotThrowAnyException();
    }

    @Test
    void findBySecretWithDomain() {
        assertThat(repository.findBySecret(Clients.Secret.RV1))
                .get()
                .hasFieldOrPropertyWithValue("secret", Clients.Secret.RV1);
    }
}
