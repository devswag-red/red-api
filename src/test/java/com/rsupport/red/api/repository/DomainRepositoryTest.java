package com.rsupport.red.api.repository;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.rsupport.red.api.domain.Domain;
import com.rsupport.red.base.Fixtures.Domains;
import com.rsupport.red.base.RepositoryTest;
import java.time.LocalDateTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

@RepositoryTest
class DomainRepositoryTest {

    @Autowired
    private DomainRepository repository;

    @Nested
    class SaveTest {

        @Test
        void save() {
            var domain = Domain.from("RV_" + LocalDateTime.now());

            assertThatCode(() -> repository.saveAndFlush(domain)).doesNotThrowAnyException();
        }

        @DisplayName("도메인 이름은 중복될 수 없다.")
        @Test
        void error() {
            var domain = Domain.from(Domains.Name.RV);

            assertThatThrownBy(() -> repository.saveAndFlush(domain))
                    .isInstanceOf(DataIntegrityViolationException.class);
        }
    }
}
