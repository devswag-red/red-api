package com.rsupport.red.api.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import com.rsupport.red.api.common.exception.BaseException;
import com.rsupport.red.api.controller.dto.response.UserResponse;
import com.rsupport.red.api.domain.User;
import com.rsupport.red.api.domain.enums.Role;
import java.util.List;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;

@DisplayName("User Service Test")
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
@SpringBootTest
class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    @Order(1)
    @DisplayName("user :: test - createUser")
    void testCreateUser() {

        userService.saveUser(User.of(
                "user1@rsupport.com",
                "user1",
                "{bcrypt}$2a$10$OhB44QvRyCP5Cnb7Tt870ePRB9cFDOGGZjviGkbZcl1JuR5uiKBZu",
                2L));
        userService.saveUser(User.of(
                "user2@rsupport.com",
                "user2",
                "{bcrypt}$2a$10$OhB44QvRyCP5Cnb7Tt870ePRB9cFDOGGZjviGkbZcl1JuR5uiKBZu",
                2L));
        userService.saveUser(User.of(
                "user3@rsupport.com",
                "user3",
                "{bcrypt}$2a$10$OhB44QvRyCP5Cnb7Tt870ePRB9cFDOGGZjviGkbZcl1JuR5uiKBZu",
                2L));

        List<UserResponse> userList = userService.getUserList(Pageable.unpaged());
        System.out.println(userList);
        userList.forEach(System.out::println);

        assertThat(5 < userList.size());
    }

    @Test
    @Order(2)
    @DisplayName("user :: test - getUser ")
    void test_GetUser() {
        User user = userService.getUserByUsername("admin@rsupport.com");

        assertAll(() -> assertThat(Role.ADMIN).isEqualTo(user.getRole()), () -> assertThat("user01")
                .isEqualTo(user.getId()));
    }

    @Test
    @Order(3)
    @DisplayName("user :: test - getUser fail, user not found")
    void test_GetUser_fail() {
        BaseException baseException =
                assertThrows(BaseException.class, () -> userService.getUserByUsername("no_one@rsupport.com"));
        assertThat(baseException.getErrorCode().equals("9003"));
    }

    @Test
    @Order(4)
    @DisplayName("user :: test - DeleteUser")
    void test_DeleteUser() {
        User targetUser = userService.getUserByUsername("user3@rsupport.com");
        userService.delete(targetUser);
        BaseException baseException =
                assertThrows(BaseException.class, () -> userService.getUserByUsername("user3@rsupport.com"));
        assertThat(baseException.getErrorCode().equals("9003"));
    }

    @Test
    @Order(5)
    @DisplayName("user :: test - DeleteUser fail, user not found")
    void test_DeleteUser_fail() {
        BaseException baseException =
                assertThrows(BaseException.class, () -> userService.getUserByUsername("user00@rsupport.com"));
        assertThat(baseException.getErrorCode().equals("9003"));
    }

    @Test
    @Order(6)
    @DisplayName("user :: test - UpdateUser")
    void test_UpdateUser() {
        User targetUser = userService.get("user99");
        targetUser.setUsername("user100@rsupport.com");
        userService.saveUser(targetUser);

        assertThat(
                userService.getUserByUsername("user100@rsupport.com").getRole().equals(Role.USER));
        BaseException baseException =
                assertThrows(BaseException.class, () -> userService.getUserByUsername("user99@rsupport.com"));
        assertThat(baseException.getErrorCode().equals("9003"));
    }

    @Test
    @Order(7)
    @DisplayName("user :: test - userList")
    void testUserList() {

        List<UserResponse> userList = userService.getUserList(Pageable.unpaged());
        System.out.println(userList);

        assertNotNull(userList);
    }
}
