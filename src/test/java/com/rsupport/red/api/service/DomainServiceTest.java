package com.rsupport.red.api.service;

import static org.assertj.core.api.Assertions.assertThatCode;

import com.rsupport.red.api.controller.dto.DomainRequest;
import com.rsupport.red.api.controller.dto.DomainRequest.SearchTarget;
import com.rsupport.red.base.AbstractServiceTest;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;

class DomainServiceTest extends AbstractServiceTest {

    @Autowired
    private DomainService service;

    @ParameterizedTest
    @MethodSource("requests")
    void findAll(SearchTarget target, String keyword) {
        DomainRequest request = new DomainRequest(target, keyword);

        assertThatCode(() -> service.findAll(request)).doesNotThrowAnyException();
    }

    static Stream<Arguments> requests() {
        return Stream.of(
                Arguments.of(null, null),
                Arguments.of(null, ""),
                Arguments.of(SearchTarget.ID, null),
                Arguments.of(SearchTarget.ID, ""),
                Arguments.of(SearchTarget.NAME, null),
                Arguments.of(SearchTarget.NAME, ""),
                Arguments.of(SearchTarget.ID, "keyword"),
                Arguments.of(SearchTarget.NAME, "keyword"));
    }
}
