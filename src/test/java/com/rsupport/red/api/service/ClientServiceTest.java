package com.rsupport.red.api.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import com.rsupport.red.api.controller.dto.ClientRequest;
import com.rsupport.red.api.controller.dto.ClientRequest.SearchTarget;
import com.rsupport.red.base.AbstractServiceTest;
import com.rsupport.red.base.Fixtures.Clients;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;

class ClientServiceTest extends AbstractServiceTest {

    @Autowired
    private ClientService service;

    @ParameterizedTest
    @MethodSource("requests")
    void findAll(SearchTarget target, String keyword) {
        var request = new ClientRequest(target, keyword);

        assertThatCode(() -> service.findAll(request)).doesNotThrowAnyException();
    }

    static Stream<Arguments> requests() {
        return Stream.of(
                Arguments.of(null, null),
                Arguments.of(null, ""),
                Arguments.of(SearchTarget.ID, null),
                Arguments.of(SearchTarget.ID, ""),
                Arguments.of(SearchTarget.SECRET, null),
                Arguments.of(SearchTarget.SECRET, ""),
                Arguments.of(SearchTarget.DOMAIN, null),
                Arguments.of(SearchTarget.DOMAIN, ""),
                Arguments.of(SearchTarget.ID, "keyword"),
                Arguments.of(SearchTarget.SECRET, "keyword"),
                Arguments.of(SearchTarget.DOMAIN, "keyword"));
    }

    @Test
    void getClient() {
        assertThat(service.getClient(Clients.Secret.RM)).hasFieldOrPropertyWithValue("id", Clients.Id.RM);
    }
}
