package com.rsupport.red.api.service;

import static org.assertj.core.api.Assertions.assertThat;

import com.rsupport.red.api.domain.MailServiceProvider;
import com.rsupport.red.base.AbstractServiceTest;
import com.rsupport.red.base.Fixtures;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Commit;

class MailServiceProviderServiceTest extends AbstractServiceTest {

    @Autowired
    private MailServiceProviderService mailServiceProviderService;

    @Test
    @Commit
    void increaseFailCount() throws Exception {
        // given
        var id = Fixtures.MailServiceProviders.Id.TEST;

        // when
        var count = 5;
        var latch = new CountDownLatch(count);
        try (var executor = Executors.newVirtualThreadPerTaskExecutor()) {
            for (int i = 0; i < count; i++) {
                executor.execute(() -> {
                    mailServiceProviderService.increaseFailCount(id);
                    latch.countDown();
                });
            }
        }
        latch.await(5, TimeUnit.SECONDS);

        var afterMailServiceProvider = mailServiceProviderService.get(id);
        assertThat(afterMailServiceProvider.getFailCount()).isEqualTo(count);

        clearMailServiceProvider(afterMailServiceProvider);
    }

    private void clearMailServiceProvider(MailServiceProvider mailServiceProvider) {
        mailServiceProvider.clearFailCount();
        mailServiceProvider.updateEnabled(true);
    }
}
