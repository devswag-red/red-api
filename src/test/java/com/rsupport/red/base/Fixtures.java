package com.rsupport.red.base;

public class Fixtures {

    public static final class Domains {

        public static final class Id {
            public static final String RV = "domain01";
            public static final String RC = "domain02";
            public static final String RM = "domain03";
        }

        public static final class Name {
            public static final String RV = "RV";
            public static final String RC = "RC";
            public static final String RM = "RM";
        }
    }

    public static final class Clients {

        public static class Id {
            public static final String RV1 = "client01";
            public static final String RV2 = "client02";
            public static final String RC = "client03";
            public static final String RM = "client04";
        }

        public static final class Secret {
            public static final String RV1 = "secret01";
            public static final String RV2 = "secret02";
            public static final String RC = "secret03";
            public static final String RM = "secret04";
        }
    }

    public static final class MailServiceProviders {
        public static class Id {
            public static final String TEST = "mail-service-provider01";
        }
    }
}
