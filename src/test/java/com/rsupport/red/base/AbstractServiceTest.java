package com.rsupport.red.base;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Transactional
@TestEnvironment
public abstract class AbstractServiceTest {}
