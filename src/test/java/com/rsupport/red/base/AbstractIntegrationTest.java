package com.rsupport.red.base;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Transactional
@TestEnvironment
@AutoConfigureMockMvc(printOnlyOnFailure = false)
public abstract class AbstractIntegrationTest {

    protected final String testAdminToken =
            "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhZG1pbkByc3VwcG9ydC5jb20iLCJ1c2VySWQiOiJ1c2VyMDEiLCJ1c2VybmFtZSI6ImFkbWluQHJzdXBwb3J0LmNvbSIsInJvbGUiOiJBRE1JTiIsImlhdCI6MTcyNDM4NzIzNCwiZXhwIjoxODEwNzg3MjM0fQ.835-M3uCaR8F4AVnZpqJA7qUiNRI6kkC6RfdM9lnwgGR15MlRdIDMpt0Tag2VBN_MG01A1Y2NEZ7zvpa_kCtTQ";

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    protected String toJsonString(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }
}
