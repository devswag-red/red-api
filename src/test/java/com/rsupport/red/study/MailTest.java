package com.rsupport.red.study;

import static org.assertj.core.api.Assertions.assertThatNoException;

import jakarta.mail.Message;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import java.util.Properties;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@Disabled
class MailTest {
    private static final String FROM_NAME = "";
    private static final String TO = "yhchoi@rsupport.com";
    private static final int PORT = 587;
    private static final String SUBJECT = "Test Subject";
    private static final String BODY = "<h1>Email Test</h1>";

    private String from = "";
    private String host = "";
    private String username = "";
    private String password = "";

    private Properties getDefaultProperties() {
        var props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.port", PORT);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.auth.login.disable", "true");
        props.put("mail.smtp.starttls.enable", "true"); // required for OCI
        props.put("mail.smtp.starttls.required", "true");
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");

        return props;
    }

    private void send() throws Exception {
        var session = Session.getDefaultInstance(getDefaultProperties());

        var msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(from, FROM_NAME));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(TO));
        msg.setSubject(SUBJECT);
        msg.setContent(BODY, "text/html");

        assertThatNoException().isThrownBy(() -> {
            Transport transport = session.getTransport();
            transport.connect(host, username, password);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
        });
    }

    @Test
    void awsSample() throws Exception {
        from = "remoteview@rsupport.com";
        host = "email-smtp.ap-northeast-2.amazonaws.com";
        username = "";
        password = "";

        send();
    }

    @Test
    void ociSample() throws Exception {
        from = "no-reply@rsup.io";
        host = "smtp.email.ap-seoul-1.oci.oraclecloud.com";
        username = "";
        password = "";

        send();
    }
}
